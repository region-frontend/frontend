# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.2.0](https://gitlab.com/region-frontend/frontend/compare/v0.1.1...v0.2.0) (2021-09-24)


### Bug Fixes

* pushing after releasing ([05d6cd2](https://gitlab.com/region-frontend/frontend/commits/05d6cd210b5e0545caf8283ba2b01b953c964592))

### 0.1.1 (2021-09-24)


### Features

* add design system package ([5903142](https://gitlab.com/region-frontend/frontend/commits/5903142ca1daadb99b9f1812416025fa6d61a37f))
* add emotion ([cfafccd](https://gitlab.com/region-frontend/frontend/commits/cfafccd06cd055b9f43add609cf36f2ee53e4c27))
* add Reatom and create User atom ([3a57844](https://gitlab.com/region-frontend/frontend/commits/3a5784402c3b2f21bc3daf02f0d9f126b987f018))
* add Router ([96fdbab](https://gitlab.com/region-frontend/frontend/commits/96fdbab0197675a81cd77d56ad9fbd22b25e9df9))
* add templates ([a6139c0](https://gitlab.com/region-frontend/frontend/commits/a6139c011ed290ed5e3627b8b0d4522d4aae25a2))
* add versioning ([85e0d1d](https://gitlab.com/region-frontend/frontend/commits/85e0d1df98b6f6c2867f8904d19047c1f502b038))
* fetch DS 1.3.0 ([8ca734d](https://gitlab.com/region-frontend/frontend/commits/8ca734d767cfc575b9e7efff485a14c68995edd0))
* **store:** add Feedbacks and Advertisements ([fff4200](https://gitlab.com/region-frontend/frontend/commits/fff42007a2a3f03b0b4daa037dda24407da3fe5a))
* update design-system to v2.0.0 ([266a32b](https://gitlab.com/region-frontend/frontend/commits/266a32badbfddccc723d7abbf8bde6adc845c722))


### Bug Fixes

* delete .idea from stage ([955f56d](https://gitlab.com/region-frontend/frontend/commits/955f56d4b8d6d4d7afe901ab3dcd832767f39e0f))
* ignore idea ([36f4f18](https://gitlab.com/region-frontend/frontend/commits/36f4f189bc5ce25e227e3b9622519ae04a1a931f))
* import MainTemplate and Container from DS ([794afc8](https://gitlab.com/region-frontend/frontend/commits/794afc89950331f55664723ded54c5da2560a538))
