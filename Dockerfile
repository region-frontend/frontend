FROM node:12-alpine as builder
WORKDIR /frontend
COPY package.json /frontend/package.json
RUN npm install --only=prod
COPY . /frontend
RUN npm run build
RUN npm install -g serve

EXPOSE 3000
CMD ["serve", "-s", "build", "-l", "3000"]
