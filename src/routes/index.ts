import { Routes } from './routes';
import { RouteType } from './types';
import { generateRouteTree, getRouterSwitch, recurseRoutes } from './utils';

export {
  getRouterSwitch,
  generateRouteTree,
  recurseRoutes,
  Routes,
};

export type {
  RouteType
};
