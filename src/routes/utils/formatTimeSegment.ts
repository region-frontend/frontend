export const formatTimeSegment = (timeSegment: number) => {
  return String(timeSegment).length === 2 ? String(timeSegment) : `0${timeSegment}`;
};
