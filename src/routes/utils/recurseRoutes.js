export const recurseRoutes = (routesTree, parentPathNest = '', template = null) => {
  const templateFinal = routesTree.template || template;
  const templateFinalWithFilter = routesTree.hasOwnProperty('template') ? routesTree.template : templateFinal;
  return routesTree.routes ?
    [
      ...(routesTree.component ? [{ path: `${parentPathNest}${routesTree.path}`, component: routesTree.component, template: templateFinalWithFilter }] : []),
      ...routesTree.routes.map(subRoute => recurseRoutes(subRoute, `${parentPathNest}${routesTree.path}`, templateFinal))
    ] :
    {
      path: `${parentPathNest}${routesTree.path}`,
      component: routesTree.component,
      template: templateFinalWithFilter,
    }
};
