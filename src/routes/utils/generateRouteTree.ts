import _ from 'lodash';
import { recurseRoutes } from './recurseRoutes';
import { RouteType } from '../types';

export const generateRouteTree = (routesTree: any[]): ReadonlyArray<RouteType> => {
  return _.flattenDeep(routesTree.map(route => recurseRoutes(route)));
};
