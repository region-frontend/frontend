import { AdvertisementRoutes, AdvertisementTemplate } from '../modules/Advertisements';
import { UserRoutes } from '../modules/User';

export const Routes = [
  {
    path: '/advertisements',
    routes: AdvertisementRoutes,
    template: AdvertisementTemplate,
  },
  {
    path: '/user',
    routes: UserRoutes,
    template: null,
  },
];
