import { feedbackStore } from "../..";

export type Resource = {
  readonly _id?: string;
  readonly category?: string;
  readonly title?: string;
  readonly description?: string;
  readonly rent_type?: string;
  readonly price?: number;
  readonly images?: ReadonlyArray<string>;
  readonly has_advertisement?: boolean;
  readonly feedbacks?: ReadonlyArray<feedbackStore.types.Resource>;
  readonly advertisement?: {
    readonly type?: string;
    readonly status?: string;
    readonly inspiration_date?: string;
    readonly expiration_date?: string;
  },
  readonly createdAt?: string;
}
