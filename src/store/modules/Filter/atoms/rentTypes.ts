import { createAtom } from '@reatom/core';
import { Requests } from '../requests';

export const rentTypes = createAtom(
  { get: () => {}, clear: () => {}, _mutate: (newState: any) => newState },
  ({ onAction, create, schedule, get }, state: any = []) => {

    onAction('_mutate', (newState) => (state = newState));
    onAction('clear', () => {
      schedule((dispatch => {
        dispatch(create('_mutate', null));
      }))
    });

    onAction('get', (token) => {
      schedule((dispatch) => {
        if (!state.length) {
          Requests.getRentTypes()
            .then(response => {
              dispatch(create('_mutate', response.data));
            });
        }
      });
    });

    return state;
  });
