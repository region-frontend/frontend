import { backend, withApiRouteGroup } from '../../../core/api';

const prefix = withApiRouteGroup(`filters`);

export const Requests = {
  getCities: () => backend.get(`${prefix}/cities`),
  getRentTypes: () => backend.get(`${prefix}/rent_types`),
  getCategories: () => backend.get(`${prefix}/categories`),
  getPrices: () => backend.get(`${prefix}/prices`),
}
