import { backend, backendWithAuth, withApiRouteGroup } from '../../../core/api';

const prefix = withApiRouteGroup(`images`);

export const Requests = {
  add: (token: string, advertId: string, data: any) => backendWithAuth(token).post(`${prefix}?advertId=${advertId}`, data),
  delete: (token: string, advertId: string, imageId: string) => backendWithAuth(token).delete(`${prefix}?advertId=${advertId}&imageId=${imageId}`),
}
