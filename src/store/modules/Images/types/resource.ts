export type Resource = {
  readonly id?: string;
  readonly user_id?: string;
  readonly comment?: string;
  readonly rating?: number;
  readonly createdAt?: string;
}
