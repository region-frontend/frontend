import { backend, backendWithAuth, withApiRouteGroup } from '../../../core/api';

const prefix = withApiRouteGroup(`adverts`);
const prefixBookmarks = withApiRouteGroup(`bookmarks`);
const prefixSearch = withApiRouteGroup(`search`);

export const Requests = {
  createAdvertisement: (token: string, data: any) => backendWithAuth(token, true).post(`${prefix}/`, data),
  getAllAdvertisements: (token: string, params: any = null) => backendWithAuth(token).get(`${prefix}/`, { params }),
  getAdvertById: (token: string, id: string) => backendWithAuth(token).get(`${prefix}/${id}`),
  updateAdvertisement: (token: string, id: number, data: any) => backendWithAuth(token).put(`${prefix}/${id}`, data),
  deleteAdvertisement: (token: string, id: number) => backendWithAuth(token).delete(`${prefix}/${id}`),
  getMyAdvertisements: (token: string) => backendWithAuth(token).get(`${prefix}/my`),
  getAllFavouritesList: (token: string) => backendWithAuth(token).get(`${prefixBookmarks}/`),
  addToFavourites: (token: string, id: string) => backendWithAuth(token).post(`${prefixBookmarks}?advertId=${id}`),
  deleteFromFavourites: (token: string, id: string) => backendWithAuth(token).delete(`${prefixBookmarks}/${id}`),
  search: (query: string, params: any = null) => backend.get(`${prefixSearch}/adverts?value=${query}`, { params }),
  autocomplete: (query: string) => backend.get(`${prefixSearch}/autocomplete?value=${query}`),
}

