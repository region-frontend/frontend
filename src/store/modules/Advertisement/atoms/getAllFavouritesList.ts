import { createAtom } from '@reatom/core';
import { Requests } from '../requests';

export const getAllFavouritesList = createAtom(
  { get: (token: string) => token, clear: () => {}, _mutate: (newState: any) => newState },
  ({ onAction, create, schedule }, state: any = []) => {

    onAction('_mutate', (newState) => (state = newState));
    onAction('clear', () => {
      schedule((dispatch => {
        dispatch(create('_mutate', null));
      }))
    });

    onAction('get', (token) => {
      schedule((dispatch) => {
        Requests.getAllFavouritesList(token)
          .then(response => {
            dispatch(create('_mutate', response.data));
          });
      });
    });

    return state;
  });
