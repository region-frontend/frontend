import { createAtom } from '@reatom/core';
import { Requests } from '../requests';

export const advertisementList = createAtom(
  { get: (token: string, params: any = null) => ({ token, params }), clear: () => {}, _mutate: (newState: any) => newState },
  ({ onAction, create, schedule, get }, state: any = []) => {

    onAction('_mutate', (newState) => (state = newState));
    onAction('clear', () => {
      schedule((dispatch => {
        dispatch(create('_mutate', null));
      }))
    });

    onAction('get', ({ token, params = {} }) => {
      state = [];
      schedule((dispatch) => {
        Requests.getAllAdvertisements(token, { ...params, status: 'Успешно' })
          .then(response => {
            dispatch(create('_mutate', response.data));
          });
      });
    });

    return state;
  });
