import { createAtom } from '@reatom/core';
import { Requests } from '../requests';

export const editAdvertisement = createAtom(
  { get: (token: string, id: string) => ({ token, id }), clear: () => {}, _mutate: (newState: any) => newState },
  ({ onAction, create, schedule }, state: any = null) => {

    onAction('_mutate', (newState) => (state = newState));
    onAction('clear', () => {
      schedule((dispatch => {
        dispatch(create('_mutate', null));
      }))
    });

    onAction('get', ({ token, id }) => {
      schedule((dispatch) => {
        Requests.getAdvertById(token, id)
          .then(response => {
            dispatch(create('_mutate', response.data));
          });
      });
    });

    return state;
  });
