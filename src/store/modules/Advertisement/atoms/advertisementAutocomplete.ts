import { createAtom } from '@reatom/core';
import { Requests } from '../requests';

export const advertisementAutocomplete = createAtom(
  { get: (query: string | null = null) => query, clear: () => {}, _mutate: (newState: any) => newState },
  ({ onAction, create, schedule, get }, state: any = []) => {

    onAction('_mutate', (newState) => (state = newState));
    onAction('clear', () => {
      schedule((dispatch => {
        dispatch(create('_mutate', null));
      }))
    });

    onAction('get', (query) => {
      state = [];
      schedule((dispatch) => {
        Requests.autocomplete(query || '')
          .then(response => {
            dispatch(create('_mutate', response.data));
          });
      });
    });

    return state;
  });
