import { createAtom } from '@reatom/core';
import { Requests } from '../requests';

export const userInfo = createAtom(
  { get: (id: number) => id, clear: () => {}, _mutate: (newState: any) => newState },
  ({ onAction, create, schedule }, state: any = null) => {

    onAction('_mutate', (newState) => (state = newState));
    onAction('clear', () => {
      schedule((dispatch => {
        dispatch(create('_mutate', null));
      }))
    });

    onAction('get', (id) => {
      schedule((dispatch) => {
        Requests.getUserById(id)
          .then(response => {
            dispatch(create('_mutate', response.data));
          });
      });
    });

    return state;
  });
