import { backend, withApiRouteGroup } from '../../../core/api';

const prefix = withApiRouteGroup(`users`);

export const Requests = {
  createUser: (data: any) => backend.post(`${prefix}/`, data),
  getAllUsers: () => backend.get(`${prefix}/`),
  getUserById: (id: number) => backend.get(`${prefix}/${id}`),
  updateUser: (id: number, data: any) => backend.put(`${prefix}/${id}`, data),
  deleteUser: (id: number) => backend.delete(`${prefix}/${id}`),
}
