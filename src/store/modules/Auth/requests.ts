import { backend, withAuthRouteGroup } from '../../../core/api';

const prefix = withAuthRouteGroup('code');

export const Requests = {
  getCode: (phone: string) => backend.post(`${prefix}`, { phone }),
  verifyCode: (code: string) => backend.post(`${prefix}/verify`, { code })
}
