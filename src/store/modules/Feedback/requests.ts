import { backend, backendWithAuth, withApiRouteGroup } from '../../../core/api';

const prefix = withApiRouteGroup(`feedback`);

export const Requests = {
  get: (token: string, advertId: string) => backendWithAuth(token).get(`${prefix}/${advertId}`),
  add: (token: string, advertId: string, data: any) => backendWithAuth(token).post(`${prefix}?advertId=${advertId}`, data)
}
