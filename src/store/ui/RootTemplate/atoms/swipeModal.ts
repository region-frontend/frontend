import { createAtom } from '@reatom/core';

export const swipeModal = createAtom(
  { set: (newState: any) => newState },
  ({ onAction }, state = null) => {

    onAction('set', (newState) => {
      state = newState;
    });

    return state;
  });
