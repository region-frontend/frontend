import { createAtom } from '@reatom/core';

export const createModal = createAtom(
  { set: (newState: boolean) => newState },
  ({ onAction }, state = false) => {

    onAction('set', (newState) => {
      state = newState;
    });

    return state;
  });
