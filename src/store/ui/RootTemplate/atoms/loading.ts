import { createAtom } from '@reatom/core';
import { advertisementList, getAllFavouritesList } from '../../../modules/Advertisement/atoms';

export const loading = createAtom(
  {
    set: (newState: boolean) => newState,
    advertisements: advertisementList,
    favourites: getAllFavouritesList,
  },
  ({ onAction, onChange }, state = false) => {

    onAction('set', (newState) => {
      state = newState;
    });

    return state;
  });
