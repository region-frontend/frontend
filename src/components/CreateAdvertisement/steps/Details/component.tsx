import React, { FC, useEffect, useState } from 'react';
import { StepProps } from '../props';
import {
  Button,
  Field,
  MaskField,
  Typography,
  typography,
  WithErrorLabel,
} from '@region-kz/design-system';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faAngleRight,
  faSlidersH,
  faFolder,
  faClock,
  faMoneyBillAlt, faComment, faPhoneAlt, faMapMarkerAlt,
} from '@fortawesome/free-solid-svg-icons';
import { Select } from '../../../Select';
import { useAtom } from '@reatom/react';
import { categories, cities, rentTypes } from '../../../../store/modules/Filter/atoms';

export const Details: FC<StepProps> = ({ changeStep, onChange, data, errors, ...rest }: StepProps) => {
  const [isSelect, setIsSelect] = useState<string | null>(null);

  const [categoryList] = useAtom(categories);
  const [cityList] = useAtom(cities);
  const [rentTypeList] = useAtom(rentTypes);

  const [category, setCategory] = useState<string | null>(data?.category || null);
  const [city, setCity] = useState<string | null>(data?.city || null);
  const [rentType, setRentType] = useState<string | null>(data?.rent_type || null);
  const [title, setTitle] = useState<string>(data?.title || '');
  const [price, setPrice] = useState<string>(data?.price || '');
  const [description, setDescription] = useState<string>(data?.description || '');
  const [phone, setPhone] = useState<string>(data?.phone || '');

  useEffect(() => {
    setCategory(data?.category);
    setCity(data?.city);
    setRentType(data?.rent_type);
    setTitle(data?.title);
    setPrice(data?.price?.toString());
    setDescription(data?.description);
    setPhone(data?.phone);
  }, [data]);

  const isDisabled = () => !(category && rentType && title?.length > 6 && price && description?.length > 30 && phone);

  const handleClick = () => {
    onChange ? onChange({
      category,
      rent_type: rentType,
      title,
      price,
      description,
      city,
      phone
    }) : void 0;
    changeStep(data?.id ? 'photosEdit' : 'photos');
  }

  return (
    <div {...rest}>
      {isSelect ? (
        (() => {
          switch (isSelect) {
            case 'city':
              return <Select title={'Город'} isSelect={setIsSelect} onChange={setCity} items={cityList} active={city} />;
            case 'category':
              return <Select title={'Категория'} isSelect={setIsSelect} onChange={setCategory} items={categoryList} active={category} />;
            case 'rentType':
              return <Select title={'Тип аренды'} isSelect={setIsSelect} onChange={setRentType} items={rentTypeList} active={rentType} />;
            default:
              return <Select title={'Категория'} isSelect={setIsSelect} onChange={setCategory} items={categoryList} active={category} />;
          }
        })()
      ) : (
        <div className={'animate__animated animate__fadeInLeft'}>
          <Typography as={'h1'} size={typography.Size.SmallHeading}>{data?.id ? 'Редактировать объявление' : 'Создайте объявление'}</Typography>

          <WithErrorLabel message={errors?.includes('category') ? 'Выберите корректную категорию под ваш транспорт' : undefined} className={`${errors?.includes('category') && 'mb-5'}`}>
            <Field
              placeholderSize={16}
              borderSize={1}
              label={'Выберите категорию'}
              type={'text'}
              value={category || ''}
              onClick={() => setIsSelect('category')}
              style={{
                fontSize: typography.Size.LargeText
              }}
              icon={
                <div className={'my-2'} style={{ fontSize: 18 }}>
                  <FontAwesomeIcon icon={faSlidersH} />
                </div>
              }
              iconSuffix={
                <div className={'my-2'} style={{ fontSize: 16 }}>
                  <FontAwesomeIcon icon={faAngleRight} />
                </div>
              }
              wrapperProps={{
                className: 'mb-3'
              }}
            />
          </WithErrorLabel>

          {category ? (
            <WithErrorLabel message={errors?.includes('city') ? 'Выберите корректный город' : undefined} className={`${errors?.includes('city') && 'mb-5'}`}>
              <Field
                placeholderSize={16}
                borderSize={1}
                label={'Выберите город'}
                type={'text'}
                value={city || ''}
                onClick={() => setIsSelect('city')}
                style={{
                  fontSize: typography.Size.LargeText
                }}
                icon={
                  <div className={'my-2'} style={{ fontSize: 18 }}>
                    <FontAwesomeIcon icon={faMapMarkerAlt} />
                  </div>
                }
                iconSuffix={
                  <div className={'my-2'} style={{ fontSize: 16 }}>
                    <FontAwesomeIcon icon={faAngleRight} />
                  </div>
                }
                wrapperProps={{
                  className: 'mb-3'
                }}
              />
            </WithErrorLabel>
          ) : null}

          {city ? (
            <WithErrorLabel message={errors?.includes('title') ? 'Заполните заголовок правильно' : undefined} className={`${errors?.includes('title') && 'mb-5'}`}>
              <Field
                placeholderSize={16}
                borderSize={1}
                label={'Заголовок объявления'}
                type={'text'}
                value={title || ''}
                onChange={(e) => {
                  setTitle(e.currentTarget.value);
                }}
                style={{
                  fontSize: typography.Size.LargeText
                }}
                icon={
                  <div className={'my-2'} style={{ fontSize: 18 }}>
                    <FontAwesomeIcon icon={faFolder} />
                  </div>
                }
                wrapperProps={{
                  className: 'mb-3'
                }}
              />
            </WithErrorLabel>
          ) : null}

          {title?.length > 6 ? (
            <WithErrorLabel message={errors?.includes('rentType') ? 'Выберите корректный тип объявления' : undefined} className={`${errors?.includes('rentType') && 'mb-5'}`}>
              <Field
                placeholderSize={16}
                borderSize={1}
                label={'Тип объявления'}
                type={'text'}
                value={rentType || ''}
                onClick={() => setIsSelect('rentType')}
                style={{
                  fontSize: typography.Size.LargeText
                }}
                icon={
                  <div className={'my-2'} style={{ fontSize: 18 }}>
                    <FontAwesomeIcon icon={faClock} />
                  </div>
                }
                iconSuffix={
                  <div className={'my-2'} style={{ fontSize: 16 }}>
                    <FontAwesomeIcon icon={faAngleRight} />
                  </div>
                }
                wrapperProps={{
                  className: 'mb-3'
                }}
              />
            </WithErrorLabel>
          ) : null}

          {rentType ? (
            <WithErrorLabel message={errors?.includes('price') ? 'Выберите корректную цену' : undefined} className={`${errors?.includes('price') && 'mb-5'}`}>
              <Field
                placeholderSize={16}
                borderSize={1}
                label={'Предложите цену'}
                type={"number"}
                value={price || ''}
                onChange={(e) => {
                  setPrice(e.currentTarget.value);
                }}
                style={{
                  fontSize: typography.Size.LargeText
                }}
                icon={
                  <div className={'my-2'} style={{ fontSize: 18 }}>
                    <FontAwesomeIcon icon={faMoneyBillAlt} />
                  </div>
                }
                wrapperProps={{
                  className: 'mb-3'
                }}
              />
            </WithErrorLabel>
          ) : null}

          {price ? (
            <WithErrorLabel message={errors?.includes('description') ? 'Введите правильное описание' : undefined} className={`${errors?.includes('description') && 'mb-5'}`}>
              <Field
                placeholderSize={16}
                borderSize={1}
                label={'Описание'}
                type={'textarea'}
                max={1800}
                value={description || ''}
                onChange={(e) => {
                  setDescription(e.currentTarget.value);
                }}
                style={{
                  fontSize: typography.Size.LargeText
                }}
                icon={
                  <div style={{ fontSize: 18, marginBottom: 33 }}>
                    <FontAwesomeIcon icon={faComment} />
                  </div>
                }
                wrapperProps={{
                  className: 'mb-3'
                }}
              />
            </WithErrorLabel>
          ) : null}

          {description?.length > 30 ? (
            <WithErrorLabel message={errors?.includes('phone') ? 'Введите правильный телефонный номер' : undefined} className={`${errors?.includes('phone') && 'mb-5'}`}>
              <MaskField
                type={'text'}
                mask={'+7(999)999-99-99'}
                style={{
                  fontSize: typography.Size.LargeText,
                }}
                borderSize={1}
                value={phone || ''}
                placeholderSize={16}
                icon={
                  <div style={{ fontSize: 18 }} className={'my-2'}>
                    <FontAwesomeIcon icon={faPhoneAlt} />
                  </div>
                }
                contentProps={{
                  style: {
                    left: 30,
                    position: 'absolute',
                    top: '50%',
                    transform: 'translateY(-50%)'
                  }
                }}
                wrapperProps={{
                  className: 'mb-3'
                }}
                onChange={(e) => {
                  setPhone(e.currentTarget.value);
                }}
              />
            </WithErrorLabel>
          ) : null}

          {!isDisabled() ? (
            <Button
              className={'p-3 mt-4 mb-4'}
              appearance={'primary'}
              style={{
                fontSize: typography.Size.RegularText,
                borderRadius: 10
              }}
              onClick={handleClick}
              disabled={isDisabled()}
              block
            >
              {isDisabled() ? 'Заполните все поля' : data?.images?.length ? 'Изменить фотографии' : 'Загрузить фотографии'}
            </Button>
          ) : null}

        </div>
      )}
    </div>
  );
}
