import React, { FC, useState } from 'react';
import { StepProps } from '../props';
import {
  Button, CheckBox,
  CircleButton,
  colors,
  TarifficationPrice,
  Typography,
  typography,
} from '@region-kz/design-system';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { DefaultHeader } from '../../../DefaultHeader';

export const Upgrade: FC<StepProps> = ({ changeStep, onChange, data, handleSubmit, ...rest }: StepProps) => {

  const [top, setTop] = useState<number | null>();
  const [fast, setFast] = useState<number | null>();
  const [turbo, setTurbo] = useState<number | null>();

  const handleClick = async () => {
    // onChange ? await onChange({
    //   upgrade: {
    //     top,
    //     fast,
    //     turbo
    //   }
    // }) : void 0;
    await handleSubmit();
  }

  const handleBack = () => {
    // onChange ? onChange({
    //   upgrade: {
    //     top,
    //     fast,
    //     turbo
    //   }
    // }) : void 0;
    changeStep('details');
  }

  return (
    <div {...rest}>
      <DefaultHeader
        withoutContainer
        navigationNode={(
          <CircleButton
            size={40}
            appearance={'default'}
            onClick={handleBack}
          >
            <FontAwesomeIcon icon={faArrowLeft} />
          </CircleButton>
        )}
      >
        <Typography style={{textAlign: 'center', fontWeight: 600}} size={20} as={'span'}>Рекламировать объявление</Typography>
      </DefaultHeader>

      <Typography
        as={'span'}
        size={typography.Size.RegularHeading}
        style={{
          fontWeight: 600
        }}
      >
        Топ-объявление
      </Typography>
      <Typography
        as={'p'}
        size={typography.Size.RegularText}
        style={{
          color: colors.Text.DarkGrey,
          margin: 0
        }}
        className={'mt-2 mb-3'}
      >
        Ваше объявление будет одним из первых в общем списке всех объявлений
      </Typography>
      <TarifficationPrice style={{ fontSize: typography.Size.LargeText }} price={350} currency={'KZT'} period={'день'} />

      <div className={'d-flex mt-4 mb-5 justify-content-between'}>
        <div style={{ display: 'flex', gap: 10, alignItems: 'center' }}>
          <CheckBox />
          <span style={{ color: colors.Text.DarkGrey }}>1 день</span>
        </div>
        <div style={{ display: 'flex', gap: 10, alignItems: 'center' }}>
          <CheckBox />
          <span style={{ color: colors.Text.DarkGrey }}>3 дня</span>
        </div>
        <div style={{ display: 'flex', gap: 10, alignItems: 'center' }}>
          <CheckBox />
          <span style={{ color: colors.Text.DarkGrey }}>1 месяц</span>
        </div>
      </div>

      <Typography
        as={'span'}
        size={typography.Size.RegularHeading}
        style={{
          fontWeight: 600
        }}
      >
        Быстрые продажи
      </Typography>
      <Typography
        as={'p'}
        size={typography.Size.RegularText}
        style={{
          color: colors.Text.DarkGrey,
          margin: 0
        }}
        className={'mt-2 mb-3'}
      >
        Ваше объявление будет выделено цветом для привлечения больше клиентов
      </Typography>
      <TarifficationPrice style={{ fontSize: typography.Size.LargeText }} price={280} currency={'KZT'} period={'день'} />

      <div className={'d-flex mt-4 mb-5 justify-content-between'}>
        <div style={{ display: 'flex', gap: 10, alignItems: 'center' }}>
          <CheckBox />
          <span style={{ color: colors.Text.DarkGrey }}>1 день</span>
        </div>
        <div style={{ display: 'flex', gap: 10, alignItems: 'center' }}>
          <CheckBox />
          <span style={{ color: colors.Text.DarkGrey }}>3 дня</span>
        </div>
        <div style={{ display: 'flex', gap: 10, alignItems: 'center' }}>
          <CheckBox />
          <span style={{ color: colors.Text.DarkGrey }}>1 месяц</span>
        </div>
      </div>

      <Typography
        as={'span'}
        size={typography.Size.RegularHeading}
        style={{
          fontWeight: 600
        }}
      >
        Турбо
      </Typography>
      <Typography
        as={'p'}
        size={typography.Size.RegularText}
        style={{
          color: colors.Text.DarkGrey,
          margin: 0
        }}
        className={'mt-2 mb-3'}
      >
        На вашем объявлении будет в 5 раз больше просмотров клиентов
      </Typography>
      <TarifficationPrice style={{ fontSize: typography.Size.LargeText }} price={320} currency={'KZT'} period={'день'} />

      <div className={'d-flex mt-4 mb-4 justify-content-between'}>
        <div style={{ display: 'flex', gap: 10, alignItems: 'center' }}>
          <CheckBox />
          <span style={{ color: colors.Text.DarkGrey }}>1 день</span>
        </div>
        <div style={{ display: 'flex', gap: 10, alignItems: 'center' }}>
          <CheckBox />
          <span style={{ color: colors.Text.DarkGrey }}>3 дня</span>
        </div>
        <div style={{ display: 'flex', gap: 10, alignItems: 'center' }}>
          <CheckBox />
          <span style={{ color: colors.Text.DarkGrey }}>1 месяц</span>
        </div>
      </div>

      <Button
        className={'p-3 mt-4'}
        appearance={'primary'}
        style={{
          fontSize: typography.Size.RegularText,
          borderRadius: 10
        }}
        onClick={() => changeStep('pay')}
        block
        disabled
      >Оплатить</Button>
      <Button
        onClick={handleClick}
        className={'mt-2 p-3 mb-5'}
        style={{ fontSize: 16 }}
        block
        appearance={'translucent'}
      >Опубликовать без рекламы</Button>
    </div>
  );
}
