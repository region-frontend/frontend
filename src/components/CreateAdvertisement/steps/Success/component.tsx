import React, { FC, useState } from 'react';
import { StepProps } from '../props';
import {
  Button, EmptyModuleSkeleton,
} from '@region-kz/design-system';
import { useHistory } from 'react-router-dom';

export const Success: FC<StepProps> = ({ changeStep, onChange, data, onClose, ...rest }: StepProps) => {
  const router = useHistory();
  const handleClick = () => {
    onClose ? onClose() : void 0;
    changeStep('details');
    router.push('/advertisements/my');
  }

  return (
    <div {...rest}>
      <EmptyModuleSkeleton
        className={'mb-4'}
        style={{ marginTop: 200 }}
        title={'Спасибо!'}
        label={'Вашу заявку проверяют наши менеджера. В скором времени вы получите уведомление о результате проверки.'}
      />
      <Button
        onClick={handleClick}
        className={'mt-2 p-3 mb-5'}
        style={{ fontSize: 16 }}
        block
        appearance={'primary'}
      >
        На главную
      </Button>
    </div>
  );
}
