import { HTMLAttributes } from 'react';

export type StepProps = Omit<HTMLAttributes<HTMLDivElement>, 'onChange'> & {
  readonly onChange?: (data: any) => void;
  readonly changeStep: (step: string) => void;
  readonly data?: any;
  readonly onClose?: () => void;
  readonly handleSubmit: () => void;
  readonly errors?: ReadonlyArray<string>;
};
