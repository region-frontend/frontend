import React, { FC } from 'react';
import { StepProps } from '../props';
import { Button, Typography, typography } from '@region-kz/design-system';

export const AddCard: FC<StepProps> = ({ changeStep, ...rest }: StepProps) => {
  return (
    <div {...rest}>
      <Typography as={'h1'} size={typography.Size.SmallHeading}>Оплата рекламы</Typography>
      <Button
        className={'p-3 mt-4'}
        appearance={'primary'}
        style={{
          fontSize: typography.Size.RegularText,
          borderRadius: 10
        }}
        onClick={() => changeStep('pay')}
        block
      >Оплатить</Button>
    </div>
  );
}
