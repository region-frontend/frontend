import React, { ChangeEventHandler, FC, useEffect, useState } from 'react';
import { StepProps } from '../props';
import {
  Button,
  CircleButton, colors,
  Grid,
  ImageCard, Spinner,
  Typography,
  typography,
  WithErrorLabel,
} from '@region-kz/design-system';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faTimes } from '@fortawesome/free-solid-svg-icons';
import { DefaultHeader } from '../../../DefaultHeader';
import { getAvatar } from '../../../../core/utils';
import { advertisementStore, imagesStore } from '../../../../store';
import { useAtom } from '@reatom/react';
import { authAtom } from '../../../../store/modules/Auth/atoms';

export const PhotosEdit: FC<StepProps> = ({ changeStep, onChange, data, errors, ...rest }: StepProps) => {

  const [dataImages, setDataImages] = useState<Array<any> | null>([]);
  const [loading, setLoading] = useState<string | null>();
  const [addLoading, setAddLoading] = useState(false);
  const [token] = useAtom(authAtom);

  const handleClick = () => {
    changeStep('upgrade');
  }

  const handleBack = () => {
    changeStep('details');
  }

  const addImage = (image: File | null = null) => {
    if (image) {
      setAddLoading(true);
      const form = new FormData();
      form.append('image', image);
      imagesStore.requests.Requests.add(token, data.id, form)
        .then(() => {
          advertisementStore.requests.Requests.getAdvertById(token, data.id)
            .then(resAdv => {
              setDataImages(resAdv.data.images);
            })
            .finally(() => setAddLoading(false));
        })
        .finally(() => setAddLoading(false));
    }
  }

  const deleteImage = (id: string) => () => {
    setLoading(id);
    imagesStore.requests.Requests.delete(token, data.id, id)
      .then(() => {
        advertisementStore.requests.Requests.getAdvertById(token, data.id)
          .then(resAdv => {
            setDataImages(resAdv.data.images);
          })
          .finally(() => setLoading(null));
      })
      .finally(() => setLoading(null));
  }

  useEffect(() => {
    setDataImages(data?.images);
  }, [data]);

  return (
    <div {...rest}>
      <DefaultHeader
        withoutContainer
        navigationNode={(
          <CircleButton
            size={40}
            appearance={'default'}
            onClick={handleBack}
          >
            <FontAwesomeIcon icon={faArrowLeft} />
          </CircleButton>
        )}
      >
        <Typography style={{textAlign: 'center', fontWeight: 600}} size={20} as={'span'}>Добавьте фотографии</Typography>
      </DefaultHeader>

      <WithErrorLabel message={errors?.includes('images') ? 'Загрузите коректные фотографии вашего транспорта' : undefined} className={`${errors?.includes('images') && 'mb-5'}`}>
        <Typography
          as={'p'}
          size={typography.Size.RegularText}
          style={{
            lineHeight: 1.4,
            margin: 0
          }}
          className={'mb-4'}
        >
          Сфотографируйте или загрузите фотографии автомобиля. Вы можете загрузить до 8-ми фотографии.
        </Typography>
      </WithErrorLabel>
      <div>
        {(dataImages || []).length ? (
          <Grid columns={2}>
            {dataImages?.map((n: any, i: number) => (
              <div className={'position-relative'}>
                <div
                  style={{
                    position: 'absolute',
                    top: -10,
                    right: -10,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    background: 'rgba(0,0,0,.8)',
                    color: '#fff',
                    borderRadius: 15,
                    width: 30,
                    height: 30,
                    zIndex: 20
                  }}
                >
                  <FontAwesomeIcon icon={faTimes} />
                </div>
                {loading === n.id ? (
                  <div
                    style={{
                      position: 'absolute',
                      top: 0,
                      right: 0,
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                      background: 'rgba(0,0,0,.8)',
                      color: '#fff',
                      borderRadius: 15,
                      width: '100%',
                      height: '100%',
                      zIndex: 20
                    }}
                  >
                    <Spinner size={5} diameter={30} color={colors.Background.White} />
                  </div>
                ) : null}
                <ImageCard style={{ height: 110 }} src={getAvatar(n?.url)} key={i} onClick={deleteImage(n.id)} />
              </div>
            ))}
            {(dataImages || []).length < 8 && !addLoading ? (
              <label
                style={{
                  height: 110,
                  width: '100%',
                  backgroundImage: `url(${getAvatar()}`,
                  backgroundSize: 'cover',
                  backgroundPosition: 'center',
                  borderRadius: 15
                }}
              >
                <input
                  type={'file'}
                  hidden={true}
                  accept="image/png, image/gif, image/jpeg"
                  onChange={(e) => addImage(e.currentTarget.files?.[0])}
                />
              </label>
            ) : null}
          </Grid>
        ) : (addLoading ? null : (
          <label
            style={{
              display: 'block',
              height: 180,
              width: '100%',
              backgroundImage: `url(${getAvatar()}`,
              backgroundSize: 'cover',
              backgroundPosition: 'center',
              borderRadius: 15
            }}
          >
            <input
              type={'file'}
              hidden={true}
              accept="image/png, image/gif, image/jpeg"
              onChange={(e) => addImage(e.currentTarget.files?.[0])}
            />
          </label>
        ))}

        {addLoading ? (
          <div style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            padding: '80px 0'
          }}>
            <Spinner diameter={50} size={4} />
          </div>
        ) : null}

      </div>
      <Button
        className={'p-3 mt-4'}
        appearance={'primary'}
        style={{
          fontSize: typography.Size.RegularText,
          borderRadius: 10
        }}
        onClick={handleClick}
        block
      >{dataImages?.length ? 'Опубликовать' : 'Пропустить'}</Button>
    </div>
  );
}
