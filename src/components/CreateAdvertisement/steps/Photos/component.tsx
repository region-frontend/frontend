import React, { FC, useEffect, useState } from 'react';
import { StepProps } from '../props';
import {
  Button,
  CircleButton,
  Grid,
  ImageCard,
  Typography,
  typography,
} from '@region-kz/design-system';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { DefaultHeader } from '../../../DefaultHeader';
import { getAvatar } from '../../../../core/utils';

export const Photos: FC<StepProps> = ({ changeStep, onChange, data, ...rest }: StepProps) => {

  const [images, setImages] = useState<FileList | null>(data?.images || null);

  const isDisabled = () => !(images?.length);

  const handleClick = () => {
    onChange ? onChange({
      images
    }) : void 0;
    changeStep('upgrade');
  }

  const handleBack = () => {
    onChange ? onChange({
      images
    }) : void 0;
    changeStep('details');
  }

  useEffect(() => {
    setImages(data?.images);
  }, [data]);

  return (
    <div {...rest}>
      <DefaultHeader
        withoutContainer
        navigationNode={(
          <CircleButton
            size={40}
            appearance={'default'}
            onClick={handleBack}
          >
            <FontAwesomeIcon icon={faArrowLeft} />
          </CircleButton>
        )}
      >
        <Typography style={{textAlign: 'center', fontWeight: 600}} size={20} as={'span'}>Добавьте фотографии</Typography>
      </DefaultHeader>

      <Typography
        as={'p'}
        size={typography.Size.RegularText}
        style={{
          lineHeight: 1.4,
          margin: 0
        }}
        className={'mb-4'}
      >
        Сфотографируйте или загрузите фотографии автомобиля. Вы можете загрузить до 8-ми фотографии.
      </Typography>
      <label>
        {images?.length ? (
          <Grid columns={2}>
            {Array.from(images).map((n: any, i) => (
              <ImageCard style={{ height: 110 }} src={URL.createObjectURL(n)} key={i} />
            ))}
            {images.length < 8 ? (
              <div
                style={{
                  height: 110,
                  width: '100%',
                  backgroundImage: `url(${getAvatar()}`,
                  backgroundSize: 'cover',
                  backgroundPosition: 'center',
                  borderRadius: 15
                }}
              />
            ) : null}
          </Grid>
        ) : (
          <div
            style={{
              height: 180,
              width: '100%',
              backgroundImage: `url(${getAvatar()}`,
              backgroundSize: 'cover',
              backgroundPosition: 'center',
              borderRadius: 15
            }}
          />
        )}
        <input
          type={'file'}
          multiple
          hidden={true}
          accept="image/png, image/gif, image/jpeg"
          onChange={(e) => setImages(e.currentTarget.files)}
        />

      </label>
      <Button
        className={'p-3 mt-4'}
        appearance={'primary'}
        style={{
          fontSize: typography.Size.RegularText,
          borderRadius: 10
        }}
        disabled={isDisabled()}
        onClick={handleClick}
        block
      >{isDisabled() ? 'Загрузите фотографии' : 'Опубликовать'}</Button>
    </div>
  );
}
