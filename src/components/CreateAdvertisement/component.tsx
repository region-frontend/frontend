import { Container } from '@region-kz/design-system';
import React, { FC, useEffect, useState } from 'react';
import { Props } from './props';
import { useAtom } from '@reatom/react';
import { authAtom } from '../../store/modules/Auth/atoms';
import { Details, Payment, Photos, Upgrade } from './steps';
import { StepProps } from './steps/props';
import { AddCard } from './steps/AddCard';
import { Success } from './steps/Success';
import { advertisementStore, imagesStore } from '../../store';
import _ from 'lodash';
import { editAdvertisement, myAdvertisements } from '../../store/modules/Advertisement/atoms';
import { PhotosEdit } from './steps/PhotosEdit';

export const CreateAdvertisement: FC<Props> = ({ onClose, ...rest }: Props) => {

  const [editItem, { clear }] = useAtom(editAdvertisement);

  const [token] = useAtom(authAtom);
  const [, { get: getMyAdvertisements }] = useAtom(myAdvertisements);
  const [step, setStep] = useState('details');

  const [data, setData] = useState<Record<any, any>>(editItem || null);

  const errors = editItem?.reason?.split(',') || [];

  useEffect(() => {
    setData(editItem);
  }, [editItem]);

  const handleSubmit = async () => {
    if (editItem) {
      const formJson = _.omit({ ...data, 'images[]': data?.images, price: Number(data.price) }, ['images', 'feedbacks', 'total_rating', 'id', 'status', 'createdAt', 'images[]']);
      advertisementStore.requests.Requests.updateAdvertisement(token, editItem.id, { ...formJson, status: 'На модерации' })
        .then(res => {
          getMyAdvertisements(token);
          setStep('success');
        })
        .finally(() => {
          clear();
        });
    } else {
      const form = new FormData();
      Object.entries(_.omit({ ...data, 'images[]': data?.images, price: Number(data.price) }, ['images', 'feedbacks', 'total_rating', 'id', 'status', 'createdAt'])).forEach(([key, value]) => {
        if (key === 'images[]') {
          Array.from(data.images).forEach((n) => {
            // @ts-ignore
            return form.append('images[]', n);
          });
        } else
          form.append(key, value);
      });
      advertisementStore.requests.Requests.createAdvertisement(token, form)
        .then(res => {
          getMyAdvertisements(token);
          setStep('success');
        })
        .finally(() => {
          clear();
        });
    }
  }

  const steps: Record<string, FC<StepProps>> = {
    details: Details,
    photos: Photos,
    photosEdit: PhotosEdit,
    upgrade: Upgrade,
    payment: Payment,
    addCard: AddCard,
    success: Success,
  };

  const Component = steps[step] || 'div';

  const handleDataPayload = (data: any) => {
    setData(prev => ({
      ...prev,
      ...data
    }));
  }

  return (
    <div {...rest}>
      <Container>
        <Component errors={errors} className={'animate__animated animate__fadeInRight'} handleSubmit={handleSubmit} data={data} onChange={handleDataPayload} changeStep={setStep} onClose={onClose} />
      </Container>
    </div>
  )
}
