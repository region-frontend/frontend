import React, { FC, useEffect, useState } from 'react';
import { Props } from './props';
import { AdCard, CircleButton, colors } from '@region-kz/design-system';
import { getAvatar } from '../../core/utils';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { advertisementStore } from '../../store/modules';
import { useAtom } from '@reatom/react';
import { uiStore } from '../../store';
import { authAtom } from '../../store/modules/Auth/atoms';
import { getAllFavouritesList } from '../../store/modules/Advertisement/atoms';
import { HeartFilled, HeartOutlined } from '@ant-design/icons';

export const Advertisement: FC<Props> = ({ item, onClick, ...rest }: Props) => {
  const [, { set: setLoading }] = useAtom(uiStore.RootTemplate.atoms.loading);
  const [token] = useAtom(authAtom);
  const [favourites] = useAtom(getAllFavouritesList);
  const [tmpFavourites, setTmpFavourites] = useState<ReadonlyArray<any>>([]);

  const addFavourite = () => {
    setLoading(true);
    advertisementStore.requests.Requests.addToFavourites(token, item.id)
      .finally(async () => {
        setTmpFavourites([...tmpFavourites, item]);
        setLoading(false);
      });
  }

  const deleteFromFavourite = () => {
    setLoading(true);
    advertisementStore.requests.Requests.deleteFromFavourites(token, item.id)
      .finally(async () => {
        setTmpFavourites(tmpFavourites.filter(n => n.id !== item.id));
        setLoading(false);
      });
  }

  useEffect(() => {
    setTmpFavourites(favourites);
  }, [favourites]);

  const isFavourite = tmpFavourites.filter((n: any) => n.id === item.id).length > 0;

  return (
    <div style={{position: 'relative'}} {...rest}>
      <AdCard onClick={onClick} borderRadius={10} className={'py-3 px-4'} price={item?.price || 0}
              currency={'KZT'} period={item?.rent_type} title={item.title} src={getAvatar(item?.images?.[0]?.url)}
              location={item?.city || ''}/>
      {token ? (
        <>
          <CircleButton
            size={40}
            appearance={'translucent'}
            onClick={isFavourite ? deleteFromFavourite : addFavourite}
            icon={
              <div style={{
                color: colors.Background.White,
                fontSize: 17,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center'
              }}>
                {isFavourite ? <HeartFilled /> : <HeartOutlined />}
              </div>
            }
            style={{position: 'absolute', top: 30, right: 40, zIndex: 10 }}
          />
        </>
      ) : null}
    </div>
  );
}
