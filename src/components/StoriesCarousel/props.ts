import { HTMLAttributes } from 'react';
import { StoryCardProps } from '@region-kz/design-system';

export type Props = Omit<HTMLAttributes<HTMLDivElement>, 'children'> & {
  readonly items: ReadonlyArray<StoryCardProps>;
}
