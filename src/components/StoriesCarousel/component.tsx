import { StoryCard } from '@region-kz/design-system';
import React, { FC } from 'react';
import { Props } from './props';
import Carousel from 'react-multi-carousel';

export const StoriesCarousel: FC<Props> = ({ items, ...rest }: Props) => {
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
      partialVisibilityGutter: 40
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
      partialVisibilityGutter: 40
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
      partialVisibilityGutter: 40
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      partialVisibilityGutter: 40
    }
  };
  return (
    <div {...rest}>
      {items.length ? (
        <Carousel
          autoPlay={false}
          responsive={responsive}
          swipeable={true}
          showDots={false}
          arrows={false}
          itemClass={'story-slide'}
          slidesToSlide={1}
          partialVisible
          minimumTouchDrag={80}
        >
          {items.map((n, i) => (
            <div style={{ padding: '0 11px' }}>
              <StoryCard style={{ height: 150, borderRadius: 10 }} src={n.src} key={i} css={undefined} />
            </div>
          ))}
        </Carousel>
      ) : null}
    </div>
  );
};
