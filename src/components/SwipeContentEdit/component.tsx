import React, { FC, useEffect, useState } from 'react';
import {
  Divider,
  ImageCard,
  Typography,
  TarifficationPrice,
  UserRating,
  Button,
  colors,
  CircleButton,
  Modal,
  Card,
  EmptyModuleSkeleton,
  WithErrorLabel,
} from '@region-kz/design-system';
import { Props } from './props';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faTrashAlt, faShareSquare } from '@fortawesome/free-solid-svg-icons';
import { getAvatar } from '../../core/utils';
import { advertisementStore } from '../../store';
import { useAtom } from '@reatom/react';
import { authAtom } from '../../store/modules/Auth/atoms';
import { DefaultHeader } from '../index';
import { Carousel } from 'react-responsive-carousel';
import { editAdvertisement, myAdvertisements } from '../../store/modules/Advertisement/atoms';
import { swipeModal } from '../../store/ui/RootTemplate/atoms';

export const SwipeContentEdit: FC<Props> = ({advertisement, fetchAdvertisement, onClose, ...rest}: Props) => {

  const [token] = useAtom(authAtom);
  const [isFeedbacks, setIsFeedbacks] = useState(false);
  const [, { get: getMyAdvertisements }] = useAtom(myAdvertisements);
  const [confirm, setConfirm] = useState(false);
  const [editItem, { get: getEditAdvertisement }] = useAtom(editAdvertisement);
  const [, { set: setSwipeModal }] = useAtom(swipeModal);

  const handleDelete = () => {
    advertisementStore.requests.Requests.deleteAdvertisement(token, advertisement?.id)
      .then(() => {
        getMyAdvertisements(token);
      })
      .finally(() => {
        onClose ? onClose() : void 0;
        setConfirm(false);
      });
  }

  const handleActivate = () => {
    advertisementStore.requests.Requests.updateAdvertisement(token, advertisement.id, { status: 'Успешно' })
      .then(() => {
        getMyAdvertisements(token);
      })
      .finally(() => {
        onClose ? onClose() : void 0;
        setConfirm(false);
      });
  }

  const handleArchive = async () => {
    advertisementStore.requests.Requests.updateAdvertisement(token, advertisement.id, { status: 'Архив' })
      .then(() => {
        getMyAdvertisements(token);
      })
      .finally(() => {
        onClose ? onClose() : void 0;
        setConfirm(false);
      });
  }

  const handleEdit = () => {
    getEditAdvertisement(token, advertisement?.id);
  }

  useEffect(() => {
    if (editItem) {
      onClose ? onClose() : void 0;
      setSwipeModal('create');
    }
  }, [editItem, onClose, setSwipeModal]);

  return (
    <div className={'p-4'} {...rest}>
      {isFeedbacks ? (
        <div className={'animate__animated animate__fadeInRight'}>
          <DefaultHeader
            noPadding
            withoutContainer
            navigationNode={(
              <CircleButton size={40} appearance={'default'} onClick={() => setIsFeedbacks(false)}>
                <FontAwesomeIcon icon={faArrowLeft} />
              </CircleButton>
            )}
          >
            <Typography style={{textAlign: 'center', fontWeight: 600}} size={20} as={'span'}>Отзывы</Typography>
          </DefaultHeader>
          {advertisement?.feedbacks?.length ? (
            (advertisement?.feedbacks || []).map((n: any, i: number) => (
              <UserRating
                className={'mt-4'}
                key={i}
                rating={{
                  score: n.rating,
                  maxScore: 5,
                  disabled: true
                }}
                comment={n.comment}
                user={{
                  avatar: {
                    url:
                      'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png',
                  },
                  name: 'Асет Салауатов',
                }}
              />
            ))
          ) : (
            <EmptyModuleSkeleton style={{ marginTop: 100 }} label={'Еще нет ни одного отзыва'} />
          )}
        </div>
      ) : (
        <div className={'animate__animated animate__fadeInLeft'}>
          <Typography style={{textAlign: 'center', fontWeight: 600}} size={20} as={'span'}>{advertisement?.title}</Typography>
          <div style={{position: 'relative'}}>
              {advertisement?.images?.length ? (
                <Carousel
                  className={'mt-4'}
                  autoPlay={false}
                  swipeable={true}
                  showArrows={false}
                  showIndicators={false}
                  showStatus={false}
                  showThumbs={false}
                >
                  {advertisement.images.map((n: any, i: number) => (
                    <ImageCard style={{ boxShadow: 'none' }} src={getAvatar(n.url)} key={i} />
                  ))}
                </Carousel>
              ) : (<ImageCard className={'mt-4'} src={getAvatar()} />)
              }

            <div style={{position: 'absolute', top: 10, right: 10, zIndex: 9999}}>
              <CircleButton
                className={'mr-2'}
                size={40}
                appearance={'translucent'}
                icon={<FontAwesomeIcon size={'lg'} color={colors.Brand.White} icon={faTrashAlt}/>}
                onClick={() => setConfirm(true)}
              />
              <CircleButton size={40} appearance={'translucent'}
                            icon={<FontAwesomeIcon size={'lg'} color={colors.Brand.White}
                                                   icon={faShareSquare}/>}
              />
            </div>
          </div>
          <div className={'mt-5'} style={{display: 'flex', justifyContent: 'space-between'}}>
            <Typography size={16} as={'span'}>Цена водителя</Typography>
            <TarifficationPrice color={colors.Text.GreySecondary} price={advertisement?.price} currency={'KZT'}
                                period={advertisement?.rent_type}/>
          </div>
          <Divider className={'mt-4'}/>
          <Typography className={'mt-4'} size={16} as={'p'}>Описание</Typography>
          <Typography
            className={'mt-2'}
            size={16}
            style={{
              lineHeight: 1.5,
              color: colors.Text.DarkGrey
            }}
            as={'span'}
          >
            {advertisement?.description}
          </Typography>

          {advertisement?.feedbacks?.length ? (
            <UserRating
              className={'mt-4'}
              rating={{
                score: advertisement?.total_rating || 0,
                maxScore: 5,
                disabled: true
              }}
              user={{
                avatar: {
                  url:
                    'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png',
                },
                name: 'Асет Салауатов',
              }}
            />
          ) : null}

          {advertisement?.status === 'На коррекцию' ? (
            <WithErrorLabel message={'Объявление не прошло модерацию, исправьте ошибки'} />
          ) : null}

          {['На модерации', 'На коррекцию'].includes(advertisement?.status)  ? (
            <Button className={'mt-4 p-3'} style={{ fontSize: 16 }} block appearance={'primary'} onClick={handleEdit}>Редактировать</Button>
          ) : null}
          {advertisement?.status === 'Архив' ? (
            <Button className={'mt-4 p-3'} style={{ fontSize: 16 }} block appearance={'primary'} onClick={handleActivate}>Активировать</Button>
          ) : null}
          {advertisement?.status === 'Успешно' ? (
            <Button className={'mt-4 p-3'} style={{ fontSize: 16 }} block appearance={'primary'} onClick={handleArchive}>Архивировать</Button>
          ) : null}
          <Button
            onClick={() => setIsFeedbacks(true)}
            className={'mt-2 p-3'}
            style={{ fontSize: 16 }}
            block
            appearance={'translucent'}
          >Отзывы</Button>
        </div>
      )}

      <Modal
        onClose={() => setConfirm(false)}
        active={confirm}
        style={{
          top: 0,
          left: 0
        }}
        bodyProps={{
          style: {
            margin: '200px 40px',
          }
        }}
      >
        <Card className={'p-3'}>
          <Typography className={'mb-4'} style={{textAlign: 'center', fontWeight: 600}} size={20} as={'h2'}>Вы действительно хотите удалить объявление?</Typography>
          <Button
            onClick={handleDelete}
            className={'mt-2 p-3'}
            style={{ fontSize: 16 }}
            block
            appearance={'primary'}
          >Подтвердить</Button>
          <Button
            onClick={() => setConfirm(false)}
            className={'mt-2 p-3'}
            style={{ fontSize: 16 }}
            block
            appearance={'translucent'}
          >Отмена</Button>
        </Card>
      </Modal>
    </div>
  );
};
