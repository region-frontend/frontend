import { Props } from './props';

export * from './component';

export type FilterProps = Props;
