import { Button, Container, Field, typography, Typography } from '@region-kz/design-system';
import React, { FC, useEffect, useState } from 'react';
import { Props } from './props';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faMapMarkerAlt,
  faSlidersH,
  faClock,
  faMoneyBillAlt,
  faAngleRight,
} from '@fortawesome/free-solid-svg-icons';
import { useAtom } from '@reatom/react';
import { categories, cities, rentTypes } from '../../store/modules/Filter/atoms';
import { prices } from '../../store/modules/Filter/atoms/prices';
import { Select } from '../Select';
import { advertisementList } from '../../store/modules/Advertisement/atoms';
import { authAtom } from '../../store/modules/Auth/atoms';

export const Filter: FC<Props> = ({ onClose, ...rest }: Props) => {

  const [, { get: getAdvertisements }] = useAtom(advertisementList);
  const [token] = useAtom(authAtom);

  const [cityList] = useAtom(cities);
  const [categoryList] = useAtom(categories);
  const [rentTypeList] = useAtom(rentTypes);
  const [priceList] = useAtom(prices);

  const [isSelect, setIsSelect] = useState<string | null>(null);

  const [city, setCity] = useState<string | null>(null);
  const [category, setCategory] = useState<string | null>(null);
  const [price, setPrice] = useState<string | null>(null);
  const [priceMin, setPriceMin] = useState<number | null>(null);
  const [priceMax, setPriceMax] = useState<number | null>(null);
  const [rentType, setRentType] = useState<string | null>(null);

  const filterQuery = {
    ...(city ? { city } : {}),
    ...(category ? { category } : {}),
    ...(priceMin ? { minPrice: priceMin } : {}),
    ...(priceMax ? { maxPrice: priceMax } : {}),
    ...(rentType ? { rent_type: rentType } : {}),
  }

  const handleFilter = async () => {
    await getAdvertisements(token, filterQuery);
    onClose ? onClose() : void 0;
  }

  const resetFilter = async () => {
    await getAdvertisements(token);
    setCity(null);
    setCategory(null);
    setPrice(null);
    setPriceMin(null);
    setPriceMax(null);
    setRentType(null);
    onClose ? onClose() : void 0;
  }

  return (
    <div {...rest}>
      <Container>
        {isSelect ? (() => {
          switch (isSelect) {
            case 'city':
              return <Select title={'Город'} isSelect={setIsSelect} onChange={setCity} items={cityList} active={city} />;
            case 'category':
              return <Select title={'Категория'} isSelect={setIsSelect} onChange={setCategory} items={categoryList} active={category} />;
            case 'price':
              return <Select title={'Цена'} isSelect={setIsSelect} onChange={setPrice} onMinChange={setPriceMin} onMaxChange={setPriceMax} items={priceList} active={price} />;
            case 'rentType':
              return <Select title={'Тип аренды'} isSelect={setIsSelect} onChange={setRentType} items={rentTypeList} active={rentType} />;
            default:
              return <Select title={'Город'} isSelect={setIsSelect} onChange={setCity} items={cityList} active={city} />;
          }
          })()
         : (
          <div className={'animate__animated animate__fadeInLeft'}>
            <Typography as={'h1'} size={typography.Size.SmallHeading}>Фильтр</Typography>
            <Field
              placeholderSize={16}
              borderSize={1}
              label={'Город'}
              type={'text'}
              value={city || ''}
              onClick={() => setIsSelect('city')}
              style={{
                fontSize: typography.Size.LargeText
              }}
              icon={
                <div className={'my-2'} style={{ fontSize: 18 }}>
                  <FontAwesomeIcon icon={faMapMarkerAlt} />
                </div>
              }
              iconSuffix={
                <div className={'my-2'} style={{ fontSize: 16 }}>
                  <FontAwesomeIcon icon={faAngleRight} />
                </div>
              }
              wrapperProps={{
                className: 'mb-3'
              }}
            />
            <Field
              placeholderSize={16}
              borderSize={1}
              label={'Выберите категорию'}
              type={'text'}
              value={category || ''}
              onClick={() => setIsSelect('category')}
              style={{
                fontSize: typography.Size.LargeText
              }}
              icon={
                <div className={'my-2'} style={{ fontSize: 18 }}>
                  <FontAwesomeIcon icon={faSlidersH} />
                </div>
              }
              iconSuffix={
                <div className={'my-2'} style={{ fontSize: 16 }}>
                  <FontAwesomeIcon icon={faAngleRight} />
                </div>
              }
              wrapperProps={{
                className: 'mb-3'
              }}
            />
            <Field
              placeholderSize={16}
              borderSize={1}
              label={'Тип аренды'}
              type={'text'}
              value={rentType || ''}
              onClick={() => setIsSelect('rentType')}
              style={{
                fontSize: typography.Size.LargeText
              }}
              icon={
                <div className={'my-2'} style={{ fontSize: 18 }}>
                  <FontAwesomeIcon icon={faClock} />
                </div>
              }
              iconSuffix={
                <div className={'my-2'} style={{ fontSize: 16 }}>
                  <FontAwesomeIcon icon={faAngleRight} />
                </div>
              }
              wrapperProps={{
                className: 'mb-3'
              }}
            />
            <Field
              placeholderSize={16}
              borderSize={1}
              label={'Цена'}
              type={'text'}
              value={price || ''}
              onClick={() => setIsSelect('price')}
              style={{
                fontSize: typography.Size.LargeText
              }}
              icon={
                <div className={'my-2'} style={{ fontSize: 18 }}>
                  <FontAwesomeIcon icon={faMoneyBillAlt} />
                </div>
              }
              iconSuffix={
                <div className={'my-2'} style={{ fontSize: 16 }}>
                  <FontAwesomeIcon icon={faAngleRight} />
                </div>
              }
              wrapperProps={{
                className: 'mb-3'
              }}
            />
            <Button
              className={'p-3 mt-4'}
              appearance={'primary'}
              style={{
                fontSize: typography.Size.RegularText,
                borderRadius: 10
              }}
              onClick={handleFilter}
              block
            >Найти</Button>
            {Object.keys(filterQuery).length ? (
              <Button
                onClick={resetFilter}
                className={'mt-2 p-3 mb-5'}
                style={{ fontSize: 16 }}
                block
                appearance={'translucent'}
              >Сбросить</Button>
            ) : null}
          </div>
        )}

      </Container>
    </div>
  )
}
