import React, { FC, useEffect, useState } from 'react';
import {
  Divider,
  ImageCard,
  Typography,
  TarifficationPrice,
  UserRating,
  Button, colors, CircleButton, FeedbackModal, EmptyModuleSkeleton,
} from '@region-kz/design-system';
import { Props } from './props';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faBars, faHeart, faShareSquare } from '@fortawesome/free-solid-svg-icons';
import { getAvatar } from '../../core/utils';
import { useHistory, useLocation } from 'react-router-dom';
import { advertisementStore, feedbackStore } from '../../store';
import { useAtom } from '@reatom/react';
import { authAtom } from '../../store/modules/Auth/atoms';
import { DefaultHeader } from '../index';
import { Carousel } from 'react-responsive-carousel';
import { HeartFilled, HeartOutlined } from '@ant-design/icons';
import { getAllFavouritesList } from '../../store/modules/Advertisement/atoms';

export const SwipeContent: FC<Props> = ({advertisement, fetchAdvertisement, ...rest}: Props) => {

  const router = useHistory();
  const [feedbackModal, setFeedbackModal] = useState(false);
  const [token] = useAtom(authAtom);
  const [isFeedbacks, setIsFeedbacks] = useState(false);
  const location = useLocation();
  const [favourites, { get: getFavourites }] = useAtom(getAllFavouritesList);

  const handleFeedback = (score: number, comment: string) => {
    feedbackStore.requests.Requests.add(token, advertisement?.id, { comment, rating: score })
      .then(res => {
        advertisement && fetchAdvertisement ? fetchAdvertisement() : void 0;
      })
      .finally(() => {
        setFeedbackModal(false);
      });
  }

  const addFavourite = () => {
    advertisementStore.requests.Requests.addToFavourites(token, advertisement.id)
      .finally(async () => {
        await getFavourites(token);
      });
  }

  const deleteFromFavourite = () => {
    advertisementStore.requests.Requests.deleteFromFavourites(token, advertisement.id)
      .finally(async () => {
        await getFavourites(token);
      });
  }


  const share = async () => {
    if (navigator?.share) {
      await navigator.share({
        title: advertisement.title,
        text: advertisement.description,
        url: window.location.origin + location.pathname + `?advertisement=${advertisement.id}`,
      })
    }
  }

  const isFavourite = favourites.filter((n: any) => n.id === advertisement?.id).length > 0;

  return (
    <div className={'p-4'} {...rest}>
      {isFeedbacks ? (
        <div className={'animate__animated animate__fadeInRight'}>
          <DefaultHeader
            noPadding
            withoutContainer
            navigationNode={(
              <CircleButton size={40} appearance={'default'} onClick={() => setIsFeedbacks(false)}>
                <FontAwesomeIcon icon={faArrowLeft} />
              </CircleButton>
            )}
          >
            <Typography style={{textAlign: 'center', fontWeight: 600}} size={20} as={'span'}>Отзывы</Typography>
          </DefaultHeader>
          {advertisement?.feedbacks?.length ? (advertisement?.feedbacks || []).map((n: any, i: number) => (
            <UserRating
              className={'mt-4'}
              key={i}
              rating={{
                score: n.rating,
                maxScore: 5,
                disabled: true
              }}
              comment={n.comment}
              user={{
                avatar: {
                  url:
                    'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png',
                },
                name: 'Асет Салауатов',
              }}
            />
          )) : (
            <EmptyModuleSkeleton style={{ marginTop: 100 }} label={'Еще нет ни одного отзыва'} />
          )}
        </div>
      ) : (
        <div className={'animate__animated animate__fadeInLeft'}>
          <Typography style={{textAlign: 'center', fontWeight: 600}} size={20} as={'span'}>{advertisement?.title}</Typography>
          <div style={{position: 'relative'}}>
              {advertisement?.images?.length ? (
                <Carousel
                  className={'mt-4'}
                  autoPlay={false}
                  swipeable={true}
                  showArrows={false}
                  showIndicators={false}
                  showStatus={false}
                  showThumbs={false}
                >
                  {advertisement.images.map((n: any, i: number) => (
                    <ImageCard style={{ boxShadow: 'none' }} src={getAvatar(n.url)} key={i} />
                  ))}
                </Carousel>
              ) : (<ImageCard className={'mt-4'} src={getAvatar()} />)
              }

            <div style={{position: 'absolute', top: 10, right: 10}}>
              <CircleButton
                className={'mr-2'}
                size={40}
                appearance={'translucent'}
                onClick={isFavourite ? deleteFromFavourite : addFavourite}
                icon={
                  <div style={{
                    color: colors.Background.White,
                    fontSize: 17,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}>
                    {isFavourite ? <HeartFilled /> : <HeartOutlined />}
                  </div>
                }
              />
              <CircleButton
                size={40}
                appearance={'translucent'}
                icon={<FontAwesomeIcon size={'lg'} color={colors.Brand.White} icon={faShareSquare}/>}
                onClick={share}
              />
            </div>
          </div>
          <div className={'mt-5'} style={{display: 'flex', justifyContent: 'space-between'}}>
            <Typography size={16} as={'span'}>Цена водителя</Typography>
            <TarifficationPrice color={colors.Text.GreySecondary} price={advertisement?.price} currency={'KZT'}
                                period={advertisement?.rent_type}/>
          </div>
          <Divider className={'mt-4'}/>
          <Typography className={'mt-4'} size={16} as={'p'}>Описание</Typography>
          <Typography
            className={'mt-2'}
            size={16}
            style={{
              lineHeight: 1.5,
              color: colors.Text.DarkGrey,
              wordBreak: 'break-all',
            }}
            as={'span'}
          >
            {advertisement?.description}
          </Typography>

          {advertisement?.feedbacks?.length ? (
            <UserRating
              className={'mt-4'}
              rating={{
                score: advertisement?.total_rating || 0,
                maxScore: 5,
                disabled: true
              }}
              user={{
                avatar: {
                  url:
                    'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png',
                },
                name: 'Асет Салауатов',
              }}
            />
          ) : null}

          <Button className={'mt-4 p-3'} style={{ fontSize: 16 }} block appearance={'primary'} onClick={() => router.push('/advertisements/messages/id')}>Написать</Button>
          <Button className={'mt-2 p-3'} style={{ fontSize: 16 }} block appearance={'translucent'} onClick={() => setFeedbackModal(prev => !prev)}>Оставить отзыв</Button>
          {advertisement?.feedbacks?.length ? (
            <Button
              onClick={() => setIsFeedbacks(true)}
              className={'mt-2 p-3'}
              style={{ fontSize: 16 }}
              block
              appearance={'translucent'}
            >Отзывы</Button>
          ) : null}

        </div>
      )}
      <FeedbackModal
        bodyProps={{
          style: {
            margin: `200px 20px`
          }
        }}
        onChange={handleFeedback}
        onClose={() => setFeedbackModal(false)}
        title={'Оцените объявление'}
        active={feedbackModal}
      />
    </div>
  );
};
