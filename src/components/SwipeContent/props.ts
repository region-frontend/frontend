import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly advertisement?: any;
  readonly fetchAdvertisement?: () => void;
};
