import React, { FC, useState } from 'react';
import { Props } from './props';
import { getAvatar } from '../../core/utils';
import { Button, Typography, typography } from '@region-kz/design-system';
import { TutorialCard } from './libs/TutorialCard';

export const Tutorial: FC<Props> = ({ onClose, ...rest }: Props) => {

  const tutorials = [
    {
      img: '/card-bg-2.png',
      title: 'Смотрите объявления по аренде автомобилей разных категорий'
    },
    {
      img: '/card-bg-3.png',
      title: 'Связывайтесь с владельцами авто в чате приложения и оценивайте объявления'
    },
    {
      img: '/card-bg-5.png',
      title: 'Добавляйте свои объявления в приложение и получайте новые предложения'
    },
  ];

  const [changed, setChanged] = useState(false);

  const [step, setStep] = useState(0);

  const ifNotLast = step < tutorials.length - 1;

  const handleClick = () => {
    if (ifNotLast) {
      setChanged(true);
      setTimeout(() => {
        setStep(prev => prev + 1);
        setChanged(false);
      }, 100);
    } else {
      onClose();
    }
  }

  return (
    <div className={`p-4 animate__animated ${changed ? 'animate__fadeOutLeft' : 'animate__fadeInRight'}`} {...rest}>
      <TutorialCard
        item={tutorials[step]}
      />

      <Button
        className={'p-3 mt-4 mb-4'}
        appearance={ifNotLast ? 'default' : 'primary'}
        style={{
          fontSize: typography.Size.RegularText,
          borderRadius: 10
        }}
        onClick={handleClick}
        block
      >
        {ifNotLast ? 'Далее' : 'Понятно'}
      </Button>
    </div>
  )
}
