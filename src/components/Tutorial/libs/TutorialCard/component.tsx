import React, { FC } from 'react';
import { Props } from './props';
import { getAvatar } from '../../../../core/utils';
import { colors, typography, Typography } from '@region-kz/design-system';

export const TutorialCard: FC<Props> = ({ item, ...rest }: Props) => {
  return (
    <div
      style={{
        display: 'flex',
        position: 'relative',
        backgroundImage: `url(${getAvatar(item.img)})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        borderRadius: 15
      }}
      {...rest}
    >
      <Typography
        as={'p'}
        size={typography.Size.RegularHeading}
        className={'p-4 m-0'}
        style={{
          margin: 0,
          position: 'absolute',
          bottom: 0,
          left: 0,
          width: '100%',
          lineHeight: 1.5,
          color: colors.Text.Light
        }}
      >{item.title}</Typography>
      <div
        style={{
          paddingTop: '100%'
        }}
      />
    </div>
  );
}
