import { HTMLAttributes, ReactNode } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly navigationNode?: ReactNode;
  readonly hintNode?: ReactNode;
  readonly notSticky?: boolean;
  readonly withoutContainer?: boolean;
  readonly noPadding?: boolean;
}
