import { Props } from './props';

export * from './component';

export type DefaultHeaderProps = Props;
