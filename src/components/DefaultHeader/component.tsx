import React, { FC } from 'react';
import { Props } from './props';
import { useAtom } from '@reatom/react';
import { uiStore } from '../../store';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { CircleButton, Container, colors, typography, Typography } from '@region-kz/design-system';

export const DefaultHeader: FC<Props> = ({ children, hintNode, navigationNode, style, notSticky, withoutContainer, noPadding, ...rest }: Props) => {
  const [, { set }] = useAtom(uiStore.RootTemplate.atoms.activeness);
  const ContainerComponent = withoutContainer ? 'div' : Container;
  return (
    <div
      style={{
        padding: noPadding ? 0 : '25px 0',
        position: notSticky ? 'relative' : 'sticky',
        top: 0,
        zIndex: 99,
        backgroundColor: colors.Background.White,
        ...style,
      }}
      {...rest}
    >
      <ContainerComponent
        style={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <div>
          {navigationNode || (
            <CircleButton size={40} appearance={'default'} onClick={() => set(true)}>
              <FontAwesomeIcon icon={faBars} />
            </CircleButton>
          )}
        </div>
        <Typography as={'h1'} size={typography.Size.SmallHeading} className={'m-0 ml-4'} style={{ flex: 1 }}>{children}</Typography>
        {hintNode}
      </ContainerComponent>
    </div>
  );
};
