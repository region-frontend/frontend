import React, { FC } from 'react';
import { Props } from './props';
import { CircleButton, colors, typography, Typography } from '@region-kz/design-system';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faCheck } from '@fortawesome/free-solid-svg-icons';
import { DefaultHeader } from '../DefaultHeader';

export const Select: FC<Props> = ({ items, onChange, isSelect, title, onMinChange, onMaxChange, active, ...rest }: Props) => {
  const handleSelect = (option: string | null) => () => {
    onChange(option);
    isSelect(null);
  }

  const handlePriceSelect = (option: string | null, min: number | null, max: number | null) => () => {
    onChange(option);
    onMinChange ? onMinChange(min) : void 0;
    onMaxChange ? onMaxChange(max) : void 0;
    isSelect(null);
  }

  return (
    <div className={'animate__animated animate__fadeInRight'} {...rest}>
      <DefaultHeader
        withoutContainer
        navigationNode={(
          <CircleButton size={40} appearance={'default'} onClick={() => isSelect(null)}>
            <FontAwesomeIcon icon={faArrowLeft} />
          </CircleButton>
        )}
      >
        <Typography style={{textAlign: 'center', fontWeight: 600}} size={20} as={'span'}>{title}</Typography>
      </DefaultHeader>
      <Typography
        as={'p'}
        size={typography.Size.LargeText}
        onClick={onMinChange ? handlePriceSelect(null, null, null) : handleSelect(null)}
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          color: !active ? colors.Brand.MainColor : colors.Text.MainBlack,
        }}
      >
        Все
        {!active ? (
          <span style={{ fontSize: typography.Size.SmallText }}>
            <FontAwesomeIcon icon={faCheck} />
          </span>
        ) : null}
      </Typography>
      {(items || []).map((n, i) => (
        <Typography
          as={'p'}
          size={typography.Size.LargeText}
          onClick={n?.min ? handlePriceSelect(n.name, n.min, n.max) : handleSelect(n?.name)}
          key={i}
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            color: n.name === active ? colors.Brand.MainColor : colors.Text.MainBlack,
          }}
        >
          {n?.name}
          {n.name === active ? (
            <span style={{ fontSize: typography.Size.SmallText }}>
            <FontAwesomeIcon icon={faCheck} />
          </span>
          ) : null}
        </Typography>
      ))}
    </div>
  );
}
