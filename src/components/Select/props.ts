import { HTMLAttributes } from 'react';

export type Props = Omit<HTMLAttributes<HTMLDivElement>, 'onChange'> & {
  readonly onChange: (newState: string | null) => void;
  readonly onMinChange?: (newState: number | null) => void;
  readonly onMaxChange?: (newState: number | null) => void;
  readonly items?: ReadonlyArray<any>;
  readonly isSelect: (newState: string | null) => void;
  readonly title: string;
  readonly active?: string | null;
}
