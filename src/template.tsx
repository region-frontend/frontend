import React, { FC, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import {
  colors,
  RootTemplate, RootTemplateProps, Spinner,
} from '@region-kz/design-system';
import { useAtom } from '@reatom/react';
import { uiStore } from './store';
import { authAtom } from './store/modules/Auth/atoms';
import { categories, cities, rentTypes } from './store/modules/Filter/atoms';
import { prices } from './store/modules/Filter/atoms/prices';
import { swipeModal } from './store/ui/RootTemplate/atoms';

type TemplateProps = Pick<RootTemplateProps, 'children'>;

export const Template: FC<TemplateProps> = (props: TemplateProps) => {
  const router = useHistory();
  const location = useLocation();
  const [activeness, { set }] = useAtom(uiStore.RootTemplate.atoms.activeness);
  const [loading] = useAtom(uiStore.RootTemplate.atoms.loading);
  const [token, { set: setToken }] = useAtom(authAtom);

  const [, { get: getCities }] = useAtom(cities);
  const [, { get: getCategories }] = useAtom(categories);
  const [, { get: getRentTypes }] = useAtom(rentTypes);
  const [, { get: getPrices }] = useAtom(prices);

  const [, { set: setSwipeModal }] = useAtom(swipeModal);


  useEffect(() => {
    if(localStorage?.getItem('authToken')) {
      // @ts-ignore
      setToken(localStorage.getItem('authToken'));
    } else {
      router.push('/user');
    }
    if(!localStorage?.getItem('isTutorialPassed')) {
      setSwipeModal('tutorial');
    }
    getCities();
    getCategories();
    getRentTypes();
    getPrices();
  }, [getCategories, getCities, getPrices, getRentTypes, router, setSwipeModal, setToken]);

  type withActiveProps = ReadonlyArray<{ path: string, label: string }>;
  const withActive = (navigationItems: withActiveProps): RootTemplateProps['navigationProps'] => {
    return navigationItems.map(n => ({
      ...n,
      isActive: location.pathname.split('/').filter(n => !!n)[0] === n.path.split('/').filter(n => !!n)[0],
      onClick: () => router.push(n.path),
    }));
  };

  return (
    <RootTemplate
      active={activeness}
      onClose={() => set(false)}
      navigationProps={withActive([
        {
          label: 'Объявления',
          path: '/advertisements',
        }
      ])}
      footerProps={withActive([
        (!token ? {
          label: 'Войти',
          path: '/user',
        } :
        {
          label: 'Выйти',
          path: '/user/logout',
        }),
      ])}
      phoneNumber={'+7 (776) 245 17 92'}
    >
      <>
        {loading ? (
          <div
            style={{
              position: 'fixed',
              zIndex: 99999,
              background: 'rgba(0,0,0,.5)',
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Spinner color={colors.Background.White} diameter={50} size={4} />
          </div>
        ) : null}
        {props.children}
      </>
    </RootTemplate>
  );
};
