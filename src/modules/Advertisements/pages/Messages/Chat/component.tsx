import React, { FC } from 'react';
import { Props } from './props';
import {
  CircleButton,
  colors,
  Container,
  MessageCloud,
  SimpleField,
} from '@region-kz/design-system';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faPaperPlane, faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { DefaultHeader } from '../../../../../components';
import { useHistory } from 'react-router-dom';

export const Chat: FC<Props> = ({advertisements, ...rest}: Props) => {
  const router = useHistory();

  const mock = [
    {
      id: 1,
      isMe: true,
      message: 'Hello',
      date: '11:00',
    },
    {
      id: 2,
      isMe: false,
      message: 'Hello, how can i help you',
      date: '11:01',
    },
    {
      id: 3,
      isMe: true,
      message: 'Да, вы успеете. Пока что мне никто не написал.',
      date: '11:01',
    },
    {
      id: 4,
      isMe: false,
      message: 'Hello, how can i help you',
      date: '11:01',
    },
    {
      id: 5,
      isMe: true,
      message: 'I wanna rent this car, is it possible?',
      date: '11:01',
    }, {
      id: 6,
      isMe: false,
      message: 'Hello, how can i help you',
      date: '11:01',
    },
    {
      id: 7,
      isMe: true,
      message: 'I wanna rent this car',
      date: '11:01',
    },{
      id: 6,
      isMe: false,
      message: 'Hello, how can i help you',
      date: '11:01',
    },{
      id: 6,
      isMe: false,
      message: 'Hello, how can i help you',
      date: '11:01',
    },{
      id: 6,
      isMe: false,
      message: 'Hello, how can i help you',
      date: '11:01',
    },{
      id: 6,
      isMe: false,
      message: 'Hello, how can i help you',
      date: '11:01',
    },{
      id: 6,
      isMe: false,
      message: 'Hello, how can i help you',
      date: '11:01',
    },{
      id: 6,
      isMe: false,
      message: 'Hello, how can i help you',
      date: '11:01',
    },{
      id: 6,
      isMe: false,
      message: 'Hello, how can i help you',
      date: '11:01',
    },{
      id: 6,
      isMe: false,
      message: 'Hello, how can i help you',
      date: '11:01',
    },
  ];

  return (
    <div style={{ height: '100%', whiteSpace: 'initial' }} {...rest}>
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%'
      }}>
        <DefaultHeader
          notSticky
          navigationNode={
            <CircleButton
              size={40}
              appearance={'translucent'}
              icon={
                <FontAwesomeIcon
                  color={colors.Brand.MainColor}
                  icon={faArrowLeft}
                />
              }
              onClick={() => router.goBack()}
            />
          }
          hintNode={
            <CircleButton
              style={{marginLeft: 10}}
              size={40}
              appearance={'primary'}
              icon={
                <FontAwesomeIcon
                  color={colors.Brand.White}
                  icon={faPhoneAlt}
                />
              }
            />
          }
        >
          Синий Bentley Continental
        </DefaultHeader>
        <Container
          style={{
            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
            width: '100%',
            overflow: 'hidden',
            flex: 1,
          }}
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              flex: 1,
              overflowY: 'auto'
            }}
          >
            {mock?.map((d) => (
              <MessageCloud
                key={d.id}
                className={'mt-4'}
                isMe={d.isMe}
                message={d.message}
                date={d.date}
              />
            ))}
          </div>
          <div className={'d-flex align-items-center justify-content-between py-3'}>
            <SimpleField
              wrapperProps={{
                style: {
                  flex: 1
                }
              }}
              withFocusStyles
            />
            <CircleButton
              style={{marginLeft: 10}}
              size={40}
              appearance={'primary'}
              icon={<FontAwesomeIcon
                size={'lg'}
                color={colors.Brand.White}
                icon={faPaperPlane}/>}
            />
          </div>
        </Container>
      </div>
    </div>
  );
};
