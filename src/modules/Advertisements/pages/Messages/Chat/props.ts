import { HTMLAttributes } from 'react';
import { advertisementStore } from '../../../../../store/modules';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly advertisements?: ReadonlyArray<advertisementStore.types.Resource>;
};
