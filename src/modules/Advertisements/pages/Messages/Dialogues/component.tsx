import React, { FC, useEffect } from 'react';
import {
  AdListing, AdListingItemProps,
  Container,
  EmptyModuleSkeleton,
} from '@region-kz/design-system';
import { Props } from './props';
import { useHistory } from 'react-router-dom';
import { DefaultHeader } from '../../../../../components';

export const Messages: FC<Props> = ({messages, ...rest}: Props) => {
  const router = useHistory();

  useEffect(() => {
    console.log(messages);
  }, [messages]);

  const items: ReadonlyArray<AdListingItemProps> = [
    {
      title: 'Синий Bentley Continental',
      description:
        'Я хотел бы арендовать на целый день ваш автомобиль. И так много текста бла бла бла',
      onClick: () => router.push('/advertisements/messages/id')
    },
    {
      title: 'Белый Bentley Continental',
      description:
        'Я хотел бы арендовать на целый день ваш автомобиль. И так много текста бла бла бла',
      onClick: () => router.push('/advertisements/messages/id')
    },
    {
      title: 'Черный BMW M5(F90)',
      description:
        'Я хотел бы арендовать на целый день ваш автомобиль. И так много текста бла бла бла',
      onClick: () => router.push('/advertisements/messages/id')
    },
    {
      title: 'Белый Nissan 350Z',
      description:
        'Я хотел бы арендовать на целый день ваш автомобиль. И так много текста бла бла бла',
      onClick: () => router.push('/advertisements/messages/id')
    },
  ];

  return (
    <div {...rest}>
      <Container className={'mt-0'}>
        {items.length ?
          <AdListing items={items}/>
          :
          <EmptyModuleSkeleton label={'У вас нет сообщении'} style={{ maxWidth: 300, margin: 'auto', marginTop: 100 }} />
        }
      </Container>
    </div>
  );
};
