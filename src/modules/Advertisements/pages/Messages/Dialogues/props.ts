import { HTMLAttributes } from 'react';
import { advertisementStore } from '../../../../../store';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly messages?: ReadonlyArray<advertisementStore.types.Resource>;
};
