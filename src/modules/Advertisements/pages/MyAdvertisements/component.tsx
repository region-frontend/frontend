import React, { FC, useState } from 'react';
import {
  AdCard,
  colors,
  Container,
  Grid,
  SwipeModal, EmptyModuleSkeleton, InPageTabulation,
} from '@region-kz/design-system';
import { Props } from './props';
import { getAvatar } from '../../../../core/utils';
import { advertisementStore } from '../../../../store/modules';
import { useAtom } from '@reatom/react';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { uiStore } from '../../../../store';
import { SwipeContentEdit } from '../../../../components/SwipeContentEdit';
import { swipeModal } from '../../../../store/ui/RootTemplate/atoms';

export const MyAdvertisements: FC<Props> = ({ advertisements, ...rest }: Props) => {
  const [swipeModalValue, { set: setSwipeModal }] = useAtom(swipeModal);

  const [token] = useAtom(authAtom);
  const [, { set: setLoading }] = useAtom(uiStore.RootTemplate.atoms.loading);

  const [advertisement, setAdvertisement] = useState<any>(null);

  const handleAdvertisementClick = (id: string) => () => {
    setLoading(true);
    advertisementStore.requests.Requests.getAdvertById(token, id)
      .then(res => {
        setAdvertisement(res.data);
        setSwipeModal('my-advertisement');
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const items = [
    {
      label: 'Активные',
      index: 'Успешно',
    },
    {
      label: 'Архив',
      index: 'Архив',
    },
    {
      label: 'На модерации',
      index: 'На модерации'
    },
  ];

  const [currentItem, setCurrentItem] = useState<any>(items[0].index);

  const filteredAdvertisements = advertisements.filter(n => {
    return (currentItem === 'На модерации') ? [currentItem, 'На коррекцию'].includes(n.status) : n.status === currentItem;
  });

  return (
    <div {...rest}>
      <Container>
        <InPageTabulation
          className={'mb-4 mt-4 py-1 px-2'}
          style={{ background: '#F5F4FF', borderRadius: 13 }}
          items={items}
          onChange={(tab) => setCurrentItem(tab)}
          currentTab={currentItem}
        />
        {filteredAdvertisements.length ?
          <>
            <Grid columns={1} gutter={20} className={'mb-5'}>
              {filteredAdvertisements.map((data, index)  =>
                <div key={index} style={{position: 'relative'}} onClick={handleAdvertisementClick(data?.id)}>
                  <AdCard borderRadius={10} className={'py-3 px-4'} price={data.price || 0}
                          currency={'KZT'} period={data?.rent_type} title={data.title} src={getAvatar(data.images?.[0]?.url)}
                          location={data?.city || ''}/>
                  {data?.status === 'На коррекцию' ? (
                    <div
                      style={{
                        position: 'absolute',
                        width: 15,
                        height: 15,
                        background: colors.Brand.Error,
                        borderRadius: 20,
                        top: -5,
                        right: -5
                      }}
                    />
                  ) : null}
                </div>
              )}
            </Grid>
          </>
          :
          <EmptyModuleSkeleton style={{ marginTop: '50%' }} label={'Здесь пока пусто'} />
        }
      </Container>

      {swipeModalValue !== 'my-advertisement' && swipeModalValue !== null ? null : (
        <SwipeModal
          drawerBleeding={300}
          switchable
          open={swipeModalValue === 'my-advertisement'}
          onClose={() => setSwipeModal(null)}
        >
          <SwipeContentEdit onClose={() => setSwipeModal(null)} advertisement={advertisement} fetchAdvertisement={handleAdvertisementClick(advertisement?.id)} />
        </SwipeModal>
      )}

    </div>
  );
};
