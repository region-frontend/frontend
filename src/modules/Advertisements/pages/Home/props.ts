import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly advertisements: ReadonlyArray<any>;
  readonly autocomplete: ReadonlyArray<any>;
  readonly search: (query: string) => void;
  readonly handleAutocomplete: (query: string) => void;
};
