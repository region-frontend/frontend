import React, { FC, useCallback, useEffect, useState } from 'react';
import {
  colors,
  Container, EmptyModuleSkeleton,
  Grid,
  SimpleField,
  SwipeModal,
  typography,
} from '@region-kz/design-system';
import { Props } from './props';
import { Advertisement, Filter, StoriesCarousel } from '../../../../components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faSlidersH } from '@fortawesome/free-solid-svg-icons';
import { storiesMock } from './mock/storiesMock';
import { SwipeContent } from '../../../../components/SwipeContent';
import { useAtom } from '@reatom/react';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { advertisementStore, uiStore } from '../../../../store';
import { swipeModal } from '../../../../store/ui/RootTemplate/atoms';
import { Tutorial } from '../../../../components/Tutorial';
import { useLocation } from 'react-router-dom';
import { CSSTransition } from 'react-transition-group';
import hexToRgba from 'hex-to-rgba';

export const Home: FC<Props> = ({ advertisements, search, autocomplete, handleAutocomplete, ...rest }: Props) => {

  const [query, setQuery] = useState('');
  const [searchFocused, setSearchFocused] = useState(false);
  const [token] = useAtom(authAtom);

  const [advertisement, setAdvertisement] = useState<any>(null);
  const [, { set: setLoading }] = useAtom(uiStore.RootTemplate.atoms.loading);
  const [swipeModalValue, { set: setSwipeModal }] = useAtom(swipeModal);

  const handleAdvertisementClick = useCallback((id: string) => () => {
    setLoading(true);
    advertisementStore.requests.Requests.getAdvertById(token, id)
      .then(res => {
        setAdvertisement(res.data);
        setSwipeModal('main-advertisement');
      })
      .finally(() => {
        setLoading(false);
      });
  }, [setLoading, setSwipeModal, token])

  const closeTutorial = () => {
    if (localStorage) {
      localStorage.setItem('isTutorialPassed', 'true');
    }
    setSwipeModal(null);
  }

  const location  = useLocation();

  useEffect(() => {
    const query = new URLSearchParams(location.search);
    const advertFromURL = query.get("advertisement");
    if (advertFromURL) {
      handleAdvertisementClick(advertFromURL)();
    }
  }, [handleAdvertisementClick, location.search]);

  return (
    <div {...rest}>
      <Container>
        <div className={'d-flex align-items-center mt-4 mb-4 position-relative'}>
          <SimpleField
            wrapperProps={{
              style: {
                fontFamily: 'inherit',
                fontSize: typography.Size.RegularText,
                padding: 15,
                flex: 1,
                position: 'relative',
                zIndex: 999
              }
            }}
            style={{
              fontSize: typography.Size.RegularText
            }}
            value={query}
            withFocusStyles
            onFocus={() => setSearchFocused(true)}
            onBlur={() => {
              setTimeout(() => setSearchFocused(false), 300);
            }}
            icon={<FontAwesomeIcon style={{ color: colors.Brand.MainColor }} icon={faSearch} />}
            placeholder={'Введите название товара/услуги'}
            onChange={e => {
              setQuery(e.currentTarget.value);
              handleAutocomplete(e.currentTarget.value);
            }}
          />
          <div
            style={{
              width: 50,
              marginLeft: 10,
              transition: 'width 0.3s, transform 0.3s, margin-left 0.3s, margin-right 0.3s',
              ...(searchFocused ? {
                width: 0,
                marginLeft: 0,
                marginRight: 0,
                transform: 'scale(0)',
                overflow: 'hidden',
              } : {})
            }}
          >
            <button
              style={{
                background: 'transparent',
                padding: '15px 0',
                width: '100%',
                border: 'none',
                fontSize: 18,
                color: colors.Brand.MainColor
              }}
              onClick={() => setSwipeModal('filter')}
            >
              <FontAwesomeIcon icon={faSlidersH} />
            </button>
          </div>

          <CSSTransition
            in={searchFocused && autocomplete?.length > 0}
            transitionEnterTimeout={300}
            transitionLeaveTimeout={300}
            unmountOnExit
            timeout={500}
            onEnter={() => false}
            onExited={() => false}
            classNames={{
              enter: 'animate__animated animate__zoomIn',
              exit: 'animate__animated animate__zoomOut'
            }}
          >
            <div style={{
              position: 'absolute',
              top: '100%',
              background: colors.Text.Light,
              padding: '15px 0',
              boxShadow: '0 1px 10px rgba(0,0,0,.1)',
              zIndex: 99,
              width: '100%',
              marginTop: 15,
              borderRadius: 10
            }}>
              {autocomplete?.map((n: any, i) => (
                <div
                  key={i}
                  onClick={() => {
                    setQuery(n.full_name);
                    search(n.full_name);
                  }}
                  css={{
                    '&:hover': {
                      background: hexToRgba(colors.Brand.MainColor, .1)
                    }
                  }}
                  className={'py-2 px-4 animate__animated animate__slideInDown'}
                >
                  {n.full_name}
                </div>
              ))}
            </div>
          </CSSTransition>
        </div>

        {/* Filter */}
        {swipeModalValue !== 'filter' && swipeModalValue !== null ? null : (
          <SwipeModal
            drawerBleeding={300}
            switchable
            open={swipeModalValue === 'filter'}
            onClose={() => setSwipeModal(null)}
          >
            <Filter onClose={() => setSwipeModal(null)} />
          </SwipeModal>
        )}
      </Container>
      {!(
        <StoriesCarousel
          className={'mb-4'}
          items={storiesMock}
        />
      )}

      <Container>
        {advertisements.length ? (
          <Grid columns={1} gutter={20} className={'mb-5'}>
            {advertisements.map((data, index) =>
              <Advertisement item={data} key={index} onClick={handleAdvertisementClick(data?.id)} />
            )}
          </Grid>
          ) :
          <EmptyModuleSkeleton style={{ marginTop: 200 }} label={'Список объявлении пуст'} />
        }

        {swipeModalValue !== 'main-advertisement' && swipeModalValue !== null ? null : (
          <SwipeModal
            drawerBleeding={300}
            switchable
            open={swipeModalValue === 'main-advertisement'}
            onClose={() => setSwipeModal(null)}
          >
            <SwipeContent advertisement={advertisement} fetchAdvertisement={handleAdvertisementClick(advertisement?.id)} />
          </SwipeModal>
        )}
      </Container>

      {swipeModalValue !== 'tutorial' && swipeModalValue !== null ? null : (
        <SwipeModal
          drawerBleeding={300}
          switchable
          open={swipeModalValue === 'tutorial'}
          onClose={closeTutorial}
        >
          <Tutorial onClose={closeTutorial} />
        </SwipeModal>
      )}
    </div>
  );
};
