import { StoryCardProps } from '@region-kz/design-system';

export const storiesMock: ReadonlyArray<StoryCardProps> = [
  {
    src: 'https://static.vecteezy.com/system/resources/previews/000/622/344/original/beautiful-background-of-lines-with-gradients-vector.jpg'
  },
  {
    src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png'
  },
  {
    src: 'https://helpx.adobe.com/content/dam/help/en/photoshop/using/convert-color-image-black-white/jcr_content/main-pars/before_and_after/image-before/Landscape-Color.jpg'
  },
  {
    src: 'https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg'
  },
]
