import React, { FC, useState } from 'react';
import {
  Container,
  Grid,
  SwipeModal, EmptyModuleSkeleton,
} from '@region-kz/design-system';
import { Props } from './props';
import { SwipeContent } from '../../../../components/SwipeContent';
import { Advertisement } from '../../../../components';
import { advertisementStore } from '../../../../store/modules';
import { useAtom } from '@reatom/react';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { uiStore } from '../../../../store';
import { swipeModal } from '../../../../store/ui/RootTemplate/atoms';

export const Favourites: FC<Props> = ({ favourites, ...rest }: Props) => {
  const [swipeModalValue, { set: setSwipeModal }] = useAtom(swipeModal);
  const [token] = useAtom(authAtom);
  const [, { set: setLoading }] = useAtom(uiStore.RootTemplate.atoms.loading);


  const [advertisement, setAdvertisement] = useState<any>(null);

  const handleAdvertisementClick = (id: string) => () => {
    setLoading(true);
    advertisementStore.requests.Requests.getAdvertById(token, id)
      .then(res => {
        setAdvertisement(res.data);
        setSwipeModal('favourite-advertisement');
      })
      .finally(() => {
        setLoading(false);
      });
  }

  return (
    <div {...rest}>
      <Container className={'mt-4'}>
        {favourites.length ?
          <>
            <Grid columns={1} gutter={20} className={'mb-5'}>
              {favourites.map((data, index)  =>
                <Advertisement item={data} key={index} onClick={handleAdvertisementClick(data?.id)} />
              )}
            </Grid>
          </>
          :
          <EmptyModuleSkeleton style={{ marginTop: '50%' }} label={'Список избранных объявлении пуст'} />
        }
      </Container>

      {swipeModalValue !== 'favourite-advertisement' && swipeModalValue !== null ? null : (
        <SwipeModal
          drawerBleeding={300}
          switchable
          open={swipeModalValue === 'favourite-advertisement'}
          onClose={() => setSwipeModal(null)}
        >
          <SwipeContent advertisement={advertisement} fetchAdvertisement={handleAdvertisementClick(advertisement?.id)} />
        </SwipeModal>
      )}

    </div>
  );
};
