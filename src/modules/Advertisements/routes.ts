import { HomeContainer, FavouritesContainer, MessagesContainer } from './containers';
import { ChatContainer } from './containers/Messages/Chat';
import { MyAdvertisementsContainer } from './containers/MyAdvertisements';

export const Routes = [
  {
    path: '/',
    component: HomeContainer,
  },
  {
    path: '/favourites',
    component: FavouritesContainer,
  },
  {
    path: '/my',
    component: MyAdvertisementsContainer,
  },
  {
    path: '/messages',
    component: MessagesContainer,
    routes: [
      {
        path: '/id',
        component: ChatContainer,
        template: null,
      }
    ]
  },
];
