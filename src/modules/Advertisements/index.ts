import { Routes } from './routes';
import { Template } from './template';

export const AdvertisementRoutes = Routes;
export const AdvertisementTemplate = Template;
