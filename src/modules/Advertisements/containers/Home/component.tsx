import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { Home } from '../../pages/Home';
import { useAtom } from '@reatom/react';
import { advertisementStore, uiStore } from '../../../../store';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import {
  advertisementAutocomplete,
  searchAdvertisements,
} from '../../../../store/modules/Advertisement/atoms';
import { useDebouncedCallback } from 'use-debounce';

export const HomeContainer: FC = () => {
  const [advertisements, { get }] = useAtom(advertisementStore.atoms.advertisementList);
  const [searchAdvertisementList, { get: searchQuery }] = useAtom(searchAdvertisements);
  const [autocomplete, { get: getAutocomplete, clear: clearAutocomplete }] = useAtom(advertisementAutocomplete);
  const [isAutocompleted, setIsAutocompleted] = useState(false);
  const [token] = useAtom(authAtom);

  useEffect(() => {
    if (token) {
      get(token);
    }
  }, [get, token]);

  const search = (query: string) => {
    setIsAutocompleted(true);
    searchQuery(query);
    clearAutocomplete();
  };

  const handleAutocomplete = useDebouncedCallback(
    (query: string) => {
      setIsAutocompleted(false);
      getAutocomplete(query);
    },
    1000
  );

  // TODO: Filter search at backend
  return (
    <Home search={search} advertisements={isAutocompleted ? searchAdvertisementList.filter((n: any) => n.status === "Успешно") : advertisements} autocomplete={autocomplete} handleAutocomplete={handleAutocomplete} />
  );
};
