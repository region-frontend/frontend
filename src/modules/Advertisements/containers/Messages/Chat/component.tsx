import React, { FC } from 'react';
import { Chat } from '../../../pages/Messages/Chat';

export const ChatContainer: FC = () => {

  return (
    <Chat />
  );
};
