import React, { FC } from 'react';
import { Messages } from '../../../pages/Messages/Dialogues';

export const MessagesContainer: FC = () => {

  return (
    <Messages />
  );
};
