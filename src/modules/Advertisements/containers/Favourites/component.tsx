import React, { FC, useEffect } from 'react';
import { useAtom } from '@reatom/react';
import { advertisementStore } from '../../../../store';
import { Favourites } from '../../pages/Favourites';
import { authAtom } from '../../../../store/modules/Auth/atoms';

export const FavouritesContainer: FC = () => {
  const [favourites, { get }] = useAtom(advertisementStore.atoms.getAllFavouritesList);
  const [token] = useAtom(authAtom);

  useEffect(() => {
    if (token){
      get(token);
    }
  }, [get, token]);

  return (
      <Favourites favourites={favourites} />
  );
};
