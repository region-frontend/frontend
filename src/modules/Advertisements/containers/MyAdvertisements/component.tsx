import React, { FC, useEffect } from 'react';
import { useAtom } from '@reatom/react';
import { advertisementStore } from '../../../../store';
import { Favourites } from '../../pages/Favourites';
import { authAtom } from '../../../../store/modules/Auth/atoms';
import { MyAdvertisements } from '../../pages/MyAdvertisements';

export const MyAdvertisementsContainer: FC = () => {
  const [advertisements, { get }] = useAtom(advertisementStore.atoms.myAdvertisements);
  const [token] = useAtom(authAtom);

  useEffect(() => {
    if (token){
      get(token);
    }
  }, [get, token]);

  return (
      <MyAdvertisements advertisements={advertisements} />
  );
};
