import React, { FC } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBox, faComment, faHeart, faHome, faPlus } from '@fortawesome/free-solid-svg-icons';
import { useHistory, useLocation } from 'react-router-dom';
import { MainTemplate, MainTemplateProps, SwipeModal } from '@region-kz/design-system';
import { utils } from '../../core';
import { CreateAdvertisement } from '../../components/CreateAdvertisement';
import { useAtom } from '@reatom/react';
import { swipeModal } from '../../store/ui/RootTemplate/atoms';
import { editAdvertisement } from '../../store/modules/Advertisement/atoms';

export const Template: FC<MainTemplateProps> = ({ navigationProps: _a, children, ...rest }: MainTemplateProps) => {
  const router = useHistory();
  const location = useLocation();
  const [, { clear }] = useAtom(editAdvertisement);
  const [swipeModalValue, { set: setSwipeModal }] = useAtom(swipeModal);


  const navigationProps = utils.moduleNavigationWithRouter((
    {
      centerItem: {
        icon: <FontAwesomeIcon icon={faPlus}/>,
        clickHandler: () => setSwipeModal('create'),
      },
      leftItems: [
        {
          icon: <FontAwesomeIcon icon={faHome}/>,
          path: '/advertisements',
        },
        {
          icon: <FontAwesomeIcon icon={faHeart}/>,
          path: '/advertisements/favourites',
        },
      ],
      rightItems: [
        {
          icon: <FontAwesomeIcon icon={faBox}/>,
          path: '/advertisements/my',
        },
        {
          icon: <FontAwesomeIcon icon={faComment}/>,
          path: '/advertisements/messages',
        },
      ],
    }
  ), router.push, location.pathname);

  const onClose = () => {
    setSwipeModal(null);
    clear();
  }


  return (
    <MainTemplate
      navigationProps={{...navigationProps, containerWidth: 1800 }}
      {...rest}
    >
      {children}
      {swipeModalValue !== 'create' && swipeModalValue !== null ? null : (
        <SwipeModal
          drawerBleeding={300}
          switchable
          open={swipeModalValue === 'create'}
          onClose={onClose}
        >
          <CreateAdvertisement onClose={onClose} />
        </SwipeModal>
      )}

    </MainTemplate>
  );
};
