import { RegistrationContainer } from './containers';
import { LogoutContainer } from './containers/Logout';

export const Routes = [
  {
    path: '/',
    component: RegistrationContainer,
  },
  {
    path: '/logout',
    component: LogoutContainer,
  }
];
