import { Button, PinCode, typography, Typography, WithErrorLabel } from '@region-kz/design-system';
import { formatTimeSegment } from '../../../../../routes/utils';
import { getMinutes, getSeconds } from '@region-kz/design-system/lib/core/utils';
import React, { FC, Fragment, ReactFragment, useEffect, useState } from 'react';
import { authStore } from '../../../../../store/modules';

export type SmsFormProps = ReactFragment & {
  readonly timerStarted: boolean;
  readonly handleSms: (pinCodeArg: number) => void;
  readonly setErrorMessage: (errorMessage: string) => void;
  readonly setReceivedSms: (newVal: string) => void;
  readonly phoneNumber: string;
  readonly errorMessage: string;
}

export const SmsForm: FC<SmsFormProps> = ({ timerStarted, handleSms, errorMessage, setErrorMessage, phoneNumber, setReceivedSms }: SmsFormProps) => {
  const [timer, setTimer] = useState<number>(60);

  const retrySms = () => {
    setTimer(60);
    authStore.requests.Requests.getCode(`+7${phoneNumber}`)
      .then((res) => {
        if (res?.data?.code) {
          setReceivedSms(res.data.code);
          console.log(res.data.code);
        }
      });
  }

  useEffect(() => {
    const interval = setInterval(() => {
      if (timerStarted) {
        if (timer > 0) {
          setTimer(time => time - 1);
        }
      }
    }, 1000);
    return () => clearInterval(interval);
  }, [timer, timerStarted]);

  return (
    <Fragment>
      <div style={{
        marginBottom: 'auto'
      }}>
        <Typography as={'h1'} size={typography.Size.LargeHeading} style={{ marginBottom: 10 }}>Введите код!</Typography>
        <Typography as={'p'} size={typography.Size.RegularText} style={{ margin: 0 }}>На номер Мы вам отправили SMS с кодом. Введите его.</Typography>
        <WithErrorLabel message={errorMessage}>
          <PinCode
            style={{
              fontSize: typography.Size.LargeHeading,
              justifyContent: 'center',
              marginTop: 15
            }}
            gutter={10}
            isPassword
            block
            slotPadding={10}
            filledHandler={handleSms}
            alignCenter
            size={20}
            length={6}
            css={{}}
            onChange={() => setErrorMessage('')}
          />
        </WithErrorLabel>
      </div>
      <Button
        className={'p-3 mt-5 mb-5'}
        appearance={'primary'}
        disabled={timer > 0}
        style={{
          fontSize: typography.Size.RegularText,
          borderRadius: 10
        }}
        onClick={retrySms}
        block
      >{timer ? `Отправить повторно через ${formatTimeSegment(getMinutes(timer*1000))}:${formatTimeSegment(getSeconds(timer*1000)%60)}` : `Отправить повторно`}</Button>
    </Fragment>
  );
};
