import logo from '../../../../../assets/images/logo.svg';
import { Button, MaskField, typography, Typography } from '@region-kz/design-system';
import React, { FC, FormEvent, Fragment, ReactFragment } from 'react';
import { authStore } from '../../../../../store';

export type PhoneFormProps = ReactFragment & {
  readonly setTimerStarted: (newVal: boolean) => void;
  readonly setStepPassed: (newVal: boolean) => void;
  readonly setPhoneNumber: (newVal: string) => void;
  readonly setReceivedSms: (newVal: string) => void;
  readonly phoneNumber: string;
};

export const PhoneForm: FC<PhoneFormProps> = ({ setTimerStarted, setStepPassed, setPhoneNumber, phoneNumber, setReceivedSms }: PhoneFormProps) => {
  const firstStep = () => {
    setStepPassed(true);
    setTimerStarted(true);
    authStore.requests.Requests.getCode(`+7${phoneNumber}`)
      .then((res) => {
        if (res?.data?.code) {
          setReceivedSms(res.data.code);
          alert(res.data.code);
        }
      });
  }

  return (
    <Fragment>
      <div style={{
        marginBottom: 'auto'
      }}>
        <div className={'text-center'}>
          <img src={logo} alt={'Region Taxi'} style={{ maxWidth: 200 }} />
        </div>
        <Typography as={'h1'} size={typography.Size.LargeHeading} style={{ marginBottom: 10 }}>Добро пожаловать!</Typography>
        <Typography as={'p'} size={typography.Size.RegularText} style={{ margin: 0 }}>Введите номер телефона чтобы зарегистрироваться.</Typography>
        <MaskField
          wrapperProps={{
            style: {
              marginTop: 20
            }
          }}
          type={'text'}
          mask={'+7(999)999-99-99'}
          style={{
            fontSize: typography.Size.LargeText
          }}
          onChange={(e: FormEvent<HTMLInputElement>) => setPhoneNumber(e.currentTarget.value)}
        />
      </div>
      <Button
        className={'p-3 mt-5 mb-5'}
        appearance={'primary'}
        disabled={phoneNumber.length < 10}
        style={{
          fontSize: typography.Size.RegularText,
          borderRadius: 10
        }}
        onClick={firstStep}
        block
      >Получить код</Button>
    </Fragment>
  );
}
