import React, { FC, useEffect, useState } from 'react';
import {
  Container,
} from '@region-kz/design-system';
import { Props } from './props';
import { PhoneForm, SmsForm } from './steps';
import { authStore } from '../../../../store';
import { useAtom } from '@reatom/react';
import { useHistory } from 'react-router-dom';

export const Registration: FC<Props> = ({ ...rest }: Props) => {
  const [phoneNumber, setPhoneNumber] = useState('');
  const [stepPassed, setStepPassed] = useState(false);
  const [timerStarted, setTimerStarted] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [receivedSms, setReceivedSms] = useState('');
  const [authAtom, { set }] = useAtom(authStore.atoms.authAtom);
  const router = useHistory();

  const handleSms = (pinCodeArg: number) => {
    // TODO: Implement authentication algorithm: Check Sms, Authenticate
    if (pinCodeArg !== Number(receivedSms)) {
      setErrorMessage('Код не верный. Попробуйте еще.');
    } else {
      authStore.requests.Requests.verifyCode(receivedSms)
        .then(async (res) => {
          if (res?.data.token) {
            set(res.data.token);
            localStorage.setItem('authToken', res.data.token);
            router.push('/advertisements');
          }
        });
    }
  }

  return (
    <div style={{
      height: '100%',
      overflowY: 'auto',
      padding: '0 20px',
      whiteSpace: 'initial'
    }} {...rest}>
      <Container style={{
        paddingTop: 150,
        display: 'flex',
        height: '100%',
        flexDirection: 'column',
      }}>
        {stepPassed ? (
          <SmsForm timerStarted={timerStarted} handleSms={handleSms} errorMessage={errorMessage} setErrorMessage={setErrorMessage} phoneNumber={phoneNumber} setReceivedSms={setReceivedSms} />
        ) : (
          <PhoneForm setTimerStarted={setTimerStarted} setStepPassed={setStepPassed} setPhoneNumber={setPhoneNumber} phoneNumber={phoneNumber} setReceivedSms={setReceivedSms} />
        )}

      </Container>
    </div>
  );
};
