import React from 'react';

import {
  BrowserRouter as Router
} from 'react-router-dom';
import { getRouterSwitch } from './routes';
import { createStore } from '@reatom/core';
import { reatomContext } from '@reatom/react';

export const App = () => {
  const RouterSwitch = getRouterSwitch();
  const store = createStore();

  return (
    <reatomContext.Provider value={store}>
      <Router>
        {RouterSwitch}
      </Router>
    </reatomContext.Provider>
  );
}

export default App;
