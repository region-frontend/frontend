import React from 'react';
import ReactDOM from 'react-dom';
import './assets/styles/index.css';
import "react-multi-carousel/lib/styles.css";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import 'bootstrap-4-grid';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'animate.css';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
