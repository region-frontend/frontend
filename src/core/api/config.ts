export enum ApiConfig {
  baseUrl = `https://api-ads.region.app`,
  apiRouteGroup = `api`,
  authRouteGroup = `auth`,
}

export const withApiRouteGroup = (endpoint: string) => `${ApiConfig.apiRouteGroup}/${endpoint}`;
export const withAuthRouteGroup = (endpoint: string) => `${ApiConfig.authRouteGroup}/${endpoint}`;
