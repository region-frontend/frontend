import axios from 'axios';
import { ApiConfig } from './config';

export const backend = axios.create({
  baseURL: ApiConfig.baseUrl,
});
