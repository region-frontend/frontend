import axios from 'axios';
import { ApiConfig } from './config';

export const backendWithAuth = (token: string, file = false) => axios.create({
  baseURL: ApiConfig.baseUrl,
  headers: {
    Authorization: `Bearer ${token}`,
    ...(file ? {
      'Content-Type': 'multipart/form-data'
    } : {}),
  }
});
