import { ModuleNavigationItemProps, ModuleNavigationProps } from '@region-kz/design-system';

type RouterItem = {
  path?: string;
  clickHandler?: (e: Event) => void,
} & Omit<ModuleNavigationItemProps, 'onClick' | 'isActive'>;

type RouterProps = {
  centerItem: RouterItem;
  leftItems: Array<RouterItem>;
  rightItems: Array<RouterItem>;
}

export const moduleNavigationWithRouter = (navigationProps: RouterProps, pushRouter: (r: any) => void, locationPathname: string): ModuleNavigationProps => {
  const isActive = (route: string) => locationPathname === route;

  const modifyItem = (item: RouterItem) => Object.entries(item).reduce(
    (accCenter, [keyCenter, valueCenter]) => (
      keyCenter === 'path' ? {
        ...accCenter,
        onClick: () => pushRouter(item.path),
        isActive: isActive(item.path || ''),
      } : {
        ...accCenter,
        [keyCenter === 'clickHandler' ? 'onClick' : keyCenter]: valueCenter
      }), {});

  // @ts-ignore
  return Object.entries(navigationProps).reduce((acc, [key, value]) => {
    return Array.isArray(value) ? {
      ...acc,
      [key]: value.map(n => modifyItem(n))
    } : {
      ...acc,
      [key]: modifyItem(value),
    };
  }, {});
};
