export const getAvatar = (url: string | null = null) => url || '/noimg.png';
